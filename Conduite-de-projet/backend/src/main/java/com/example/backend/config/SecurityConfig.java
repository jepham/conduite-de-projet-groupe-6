package com.example.backend.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SecurityConfig {
    
    // Configurer la sécurité avec SecurityFilterChain
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.csrf().disable()
            .authorizeRequests()
            .requestMatchers("/**").permitAll()  // Remplacez antMatchers par requestMatchers
            .anyRequest().authenticated()  // Exige une authentification pour toutes les autres requêtes
            .and()
            .httpBasic();  // Utilisation de l'authentification basique HTTP
        return http.build();
    }

    // Définir le PasswordEncoder pour l'encodage des mots de passe
    @Bean
    public PasswordEncoder passwordEncoder() {
        System.out.println("Creating PasswordEncoder bean...");
        return new BCryptPasswordEncoder();  // Utilisation de BCrypt pour l'encodage des mots de passe
    }
}