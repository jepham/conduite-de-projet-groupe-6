package com.example.backend.controller;

import com.example.backend.model.User;
import com.example.backend.service.AuthentificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/auth")
public class AuthentificationController {

    private final AuthentificationService authentificationService;

    @Autowired
    public AuthentificationController(AuthentificationService authentificationService) {
        this.authentificationService = authentificationService;
    }

    // Endpoint pour se connecter
    @PostMapping("/login")
    public Map<String, Object> login(@RequestParam String username, @RequestParam String password) {
        // Vérification de l'authentification
        boolean isAuthenticated = authentificationService.authenticate(username, password);

        if (isAuthenticated) {
            // Récupérer l'utilisateur à partir du nom d'utilisateur
            User user = authentificationService.getUserByUsername(username);
            // Générer le token JWT pour l'utilisateur
            String token = authentificationService.generateToken(user);

            // Créer une réponse avec les informations nécessaires
            Map<String, Object> response = new HashMap<>();
            response.put("token", token);  // Ajout du token
            response.put("username", user.getUsername());  // Ajout du nom d'utilisateur
            response.put("email", user.getEmail());  // Ajout de l'email si nécessaire

            return response;
        } else {
            throw new RuntimeException("Invalid credentials");  // Retourner une exception si l'authentification échoue
        }
    }

    // Endpoint pour s'inscrire
    @PostMapping("/register")
    public User register(@RequestBody User user) {
        return authentificationService.register(user);  // Enregistrer un nouvel utilisateur
    }
}