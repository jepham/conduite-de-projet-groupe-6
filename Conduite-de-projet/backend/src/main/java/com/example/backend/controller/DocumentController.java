package com.example.backend.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.backend.model.Document;
import com.example.backend.model.FileType;
import com.example.backend.service.DocumentService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/projects/{projectId}/documents")
public class DocumentController {

    private final DocumentService documentService;

    public DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @GetMapping
    public ResponseEntity<List<Document>> getDocumentsByProject(@PathVariable Long projectId) {
        return ResponseEntity.ok(documentService.findByProject(projectId));
    }

    @PostMapping
    public ResponseEntity<Document> createDocument(@PathVariable Long projectId, @Valid @RequestBody Document document) {
        return ResponseEntity.ok(documentService.createDocument(projectId, document));
    }

    @PostMapping("/upload")
    public ResponseEntity<Document> uploadDocument(@PathVariable Long projectId,
            @RequestParam("file") MultipartFile file,
            @RequestParam("fileType") FileType fileType) {
        return ResponseEntity.ok(documentService.uploadDocument(projectId, file, fileType));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Document> updateDocument(@PathVariable Long projectId, @PathVariable Long id, @Valid @RequestBody Document document) {
        return ResponseEntity.ok(documentService.updateDocument(projectId, id, document));
    }

    @PostMapping("/archive/{id}")
    public ResponseEntity<Void> archiveDocument(@PathVariable Long id) {
        documentService.archiveDocument(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/unarchive/{id}")
    public ResponseEntity<Void> unarchiveDocument(@PathVariable Long id) {
        documentService.unarchiveDocument(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteDocument(@PathVariable Long id) {
        documentService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handleRuntimeException(RuntimeException e) {
        return ResponseEntity.badRequest().body(e.getMessage());
    }
}
