package com.example.backend.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.backend.model.FileEntity;
import com.example.backend.model.FileType;
import com.example.backend.model.Issue;
import com.example.backend.service.FileService;

@RestController
@RequestMapping("/api")
public class FileController {

    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping("/{projectId}/files/{type}")
    public ResponseEntity<String> uploadFile(
            @PathVariable Long projectId,
            @PathVariable FileType type,
            @RequestParam("file") MultipartFile file,
            @RequestParam(value = "name", required = false) String customName,
            @RequestParam("createdAt") String createdAt) {
        try {
            FileEntity savedFile = fileService.saveFile(file, projectId, type, createdAt, customName);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body("File uploaded successfully! ID: " + savedFile.getId());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("File upload failed: " + e.getMessage());
        }
    }

    @GetMapping("/projects/{projectId}/files/{type}")
    public ResponseEntity<List<FileEntity>> getFilesByType(
            @PathVariable Long projectId,
            @PathVariable FileType type) {
        try {
            List<FileEntity> files = fileService.getFilesByType(projectId, type);
            return ResponseEntity.ok(files);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(List.of());
        }
    }

    @GetMapping("/projects/{projectId}/files/{type}/{fileId}")
    public ResponseEntity<? extends Object> getFile(
            @PathVariable Long projectId,
            @PathVariable FileType type,
            @PathVariable Long fileId) {
        return fileService.getFile(fileId).map(file -> {
            if (!file.getType().equals(type)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(org.springframework.http.MediaType.parseMediaType(file.getContentType()));
            headers.setContentDispositionFormData("attachment", file.getName());
            return new ResponseEntity<>(file.getData(), headers, HttpStatus.OK);
        }).orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));
    }

    @DeleteMapping("/projects/{projectId}/files/{type}/{fileId}")
    public ResponseEntity<?> deleteFile(
            @PathVariable Long projectId,
            @PathVariable FileType type,
            @PathVariable Long fileId) {
        try {
            Optional<FileEntity> fileEntity = fileService.getFile(fileId);
            if (fileEntity.isPresent() && fileEntity.get().getType().equals(type)) {
                fileService.deleteFile(fileId);
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("File type mismatch or file not found.");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Failed to delete file: " + e.getMessage());
        }
    }

    @PostMapping("/issues/{issueId}/files/{fileId}")
    public ResponseEntity<FileEntity> associateFileWithIssue(
            @PathVariable Long issueId,
            @PathVariable Long fileId) {
        try {
            FileEntity associatedFile = fileService.associateFileWithIssue(fileId, issueId);
            return ResponseEntity.ok(associatedFile);
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @DeleteMapping("/issues/{issueId}/files/{fileId}")
    public ResponseEntity<?> removeFileFromIssue(
            @PathVariable Long issueId,
            @PathVariable Long fileId) {
        try {
            fileService.removeFileFromIssue(fileId, issueId);
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Failed to remove file from issue: " + e.getMessage());
        }
    }

    @GetMapping("/issues/{issueId}/files")
    public ResponseEntity<List<FileEntity>> getFilesByIssue(
            @PathVariable Long issueId) {
        try {
            List<FileEntity> files = fileService.getFilesByIssue(issueId);
            return ResponseEntity.ok(files);
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping("/files/{fileId}/issues")
    public ResponseEntity<List<Issue>> getIssuesByFile(
            @PathVariable Long fileId) {
        try {
            List<Issue> issues = fileService.getIssuesByFile(fileId);
            return ResponseEntity.ok(issues);
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PutMapping("/files/{fileId}")
    public ResponseEntity<FileEntity> updateFileMetadata(
            @PathVariable Long fileId,
            @RequestBody FileEntity updatedFile) {
        try {
            FileEntity file = fileService.updateFileMetadata(fileId, updatedFile);
            return ResponseEntity.ok(file);
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}
