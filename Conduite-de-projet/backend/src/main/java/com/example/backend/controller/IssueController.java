package com.example.backend.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.backend.model.Issue;
import com.example.backend.service.IssueService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/projects")
public class IssueController {

    private final IssueService issueService;

    public IssueController(IssueService issueService) {
        this.issueService = issueService;
    }

    @GetMapping("/{projectId}/issues/priority/{priority}")
    public List<Issue> getIssuesByPriority(@PathVariable String priority) {
        return issueService.findByPriority(priority);
    }

    @PostMapping("/{projectId}/issues") // Create a new issue for a specific project
    public Issue createIssue(@PathVariable Long projectId, @Valid @RequestBody Issue issue) {
        return issueService.createIssue(projectId, issue);
    }

    @PutMapping("/{projectId}/issues/{id}") // Update an issue
    public ResponseEntity<Issue> updateIssue(@PathVariable Long projectId, @PathVariable Long id, @Valid @RequestBody Issue issue) {
        return ResponseEntity.ok(issueService.update(id, issue));
    }

    @DeleteMapping("/{projectId}/issues/{id}") // Delete an issue
    public ResponseEntity<?> deleteIssue(@PathVariable Long projectId, @PathVariable Long id) {
        issueService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{projectId}/issues")
    public List<Issue> getAllIssues(@PathVariable Long projectId) {
        return issueService.getAllIssues(projectId);
    }

    @GetMapping("/{projectId}/issues/open")
    public List<Issue> getOpenIssues() {
        return issueService.findOpenIssues();
    }
}
