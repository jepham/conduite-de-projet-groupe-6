package com.example.backend.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.backend.model.Task;
import com.example.backend.model.TaskDTO;
import com.example.backend.service.TaskService;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/projects") // Change the base path to /projects
public class TaskController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/{projectId}/tasks") // Get tasks for a specific project
    public List<Task> getTasksByProject(@PathVariable Long projectId) {
        return taskService.getAllTasks(projectId);
    }

    @PostMapping("/{projectId}/tasks") // Create a task for a specific project
    public Task createTask(@PathVariable Long projectId,
            @Valid @RequestBody TaskDTO taskDTO) {
        return taskService.createTask(projectId, taskDTO);
    }

    @PutMapping("/{projectId}/tasks/{id}") // Update a specific task
    public ResponseEntity<Task> updateTask(@PathVariable Long projectId, @PathVariable Long id,
            @Valid @RequestBody TaskDTO taskDTO) {
        return ResponseEntity.ok(taskService.update(id, taskDTO));
    }

    @DeleteMapping("/{projectId}/tasks/{id}")
    public ResponseEntity<?> deleteTask(@PathVariable Long projectId, @PathVariable Long id) {
        taskService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{projectId}/tasks/completed") // Get completed tasks for a specific project
    public List<Task> getCompletedTasks(@PathVariable Long projectId) {
        return taskService.getCompletedTasks(projectId);
    }

    @GetMapping("/{projectId}/tasks/pending") // Get pending tasks for a specific project
    public List<Task> getPendingTasks(@PathVariable Long projectId) {
        return taskService.getPendingTasks(projectId);
    }

    @GetMapping("/{projectId}/tasks/progress") // Get pending tasks for a specific project
    public List<Task> getInProgressTasks(@PathVariable Long projectId) {
        return taskService.getInProgressTasks(projectId);
    }
}
