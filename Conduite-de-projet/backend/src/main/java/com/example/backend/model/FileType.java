package com.example.backend.model;

public enum FileType {
    DOCUMENT,
    ARCHIVE
}
