package com.example.backend.model;


public enum TestStatus {
    NOT_STARTED, IN_PROGRESS, PASSED, FAILED
}