package com.example.backend.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.backend.model.Document;

@Repository
public interface DocumentRepo extends JpaRepository<Document, Long> {

    List<Document> findByProjectId(Long projectId);

    Optional<Document> findByProjectIdAndName(Long projectId, String name);

}
