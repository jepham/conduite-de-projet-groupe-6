package com.example.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.backend.model.FileEntity;
import com.example.backend.model.FileType;

public interface FileRepo extends JpaRepository<FileEntity, Long> {

    List<FileEntity> findByProjectIdAndType(Long projectId, FileType type);
}
