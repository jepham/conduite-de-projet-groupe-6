package com.example.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.backend.model.FileEntity;
import com.example.backend.model.Issue;

@Repository
public interface IssueRepo extends JpaRepository<Issue, Long> {

    List<Issue> findByProjectId(Long projectId);

    public List<Issue> findByPriority(String priority);

    public List<Issue> findByStatus(String open);

    List<Issue> findByFiles(FileEntity file);

    List<Issue> findByProjectIdAndStatus(Long projectId, String status);

}
