package com.example.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.backend.model.Project;
import com.example.backend.model.ProjectStatus;

@Repository
public interface ProjectRepo extends JpaRepository<Project, Long> {

    List<Project> findByStatus(ProjectStatus status);

    @Query("SELECT DISTINCT p FROM Project p JOIN FETCH p.users u LEFT JOIN FETCH p.tasks WHERE u.id = :userId")
    List<Project> findByUserId(@Param("userId") Long userId);

    @Query("SELECT DISTINCT p FROM Project p JOIN FETCH p.users u LEFT JOIN FETCH p.tasks WHERE u.username = :username")
    List<Project> findByUsername(@Param("username") String username);
}
