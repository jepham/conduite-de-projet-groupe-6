package com.example.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.backend.model.Task;
import com.example.backend.model.TaskStatus;

@Repository
public interface TaskRepo extends JpaRepository<Task, Long> {

    // Find all tasks by project ID
    List<Task> findByProjectId(Long projectId);

    // Find tasks by project ID and status
    List<Task> findByProjectIdAndStatus(Long projectId, TaskStatus status);
}
