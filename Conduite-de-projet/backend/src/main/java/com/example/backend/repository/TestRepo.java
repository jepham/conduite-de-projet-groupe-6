package com.example.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.backend.model.Test;
import com.example.backend.model.TestStatus;

@Repository
public interface TestRepo extends JpaRepository<Test, Long> {

    List<Test> findByProjectId(Long projectId);

    List<Test> findByStatus(TestStatus status);
}
