package com.example.backend.service;

import com.example.backend.model.User;
import com.example.backend.repository.AuthentificationRepo;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import javax.crypto.SecretKey;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class AuthentificationService {

    private final AuthentificationRepo authentificationRepo;
    private final PasswordEncoder passwordEncoder; // Utilisez PasswordEncoder ici

    // Génération d'une clé secrète sécurisée
    private SecretKey secretKey = Keys.secretKeyFor(SignatureAlgorithm.HS512);  // Génération automatique d'une clé sécurisée pour HS512

    public AuthentificationService(AuthentificationRepo authentificationRepo, PasswordEncoder passwordEncoder) {
        this.authentificationRepo = authentificationRepo;
        this.passwordEncoder = passwordEncoder;
    }

    // Méthode d'authentification qui vérifie les credentials
    public boolean authenticate(String username, String password) {
        Optional<User> userOpt = authentificationRepo.findByUsername(username);
        if (userOpt.isPresent()) {
            User user = userOpt.get();
            boolean passwordMatches = passwordEncoder.matches(password, user.getPassword());
            return passwordMatches;
        }
        return false;  // Utilisateur non trouvé ou mot de passe incorrect
    }

    // Génération du token JWT à partir de l'utilisateur
    public String generateToken(User user) {
        return Jwts.builder()
                .setSubject(user.getUsername()) // Nom d'utilisateur comme sujet
                .setIssuedAt(new Date()) // Date d'émission
                .setExpiration(new Date(System.currentTimeMillis() + 86400000)) // Date d'expiration (24 heures par exemple)
                .signWith(secretKey) // Signature avec la clé générée automatiquement
                .compact(); // Génération du token
    }

    // Méthode pour inscrire un nouvel utilisateur
    public User register(User user) {
        // Vérifier si le nom d'utilisateur existe déjà
        if (authentificationRepo.findByUsername(user.getUsername()).isPresent()) {
            throw new RuntimeException("Username already taken");
        }

        // Vérifier si l'email existe déjà
        if (authentificationRepo.findByEmail(user.getEmail()).isPresent()) {
            throw new RuntimeException("Email already taken");
        }

        // Encoder le mot de passe avant de l'enregistrer
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return authentificationRepo.save(user); // Sauvegarder l'utilisateur
    }

    // Récupérer un utilisateur par son nom d'utilisateur
    public User getUserByUsername(String username) {
        Optional<User> user = authentificationRepo.findByUsername(username);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new RuntimeException("User not found with username: " + username);
        }
    }
}