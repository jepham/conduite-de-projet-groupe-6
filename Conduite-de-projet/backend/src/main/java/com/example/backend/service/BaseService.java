package com.example.backend.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public abstract class BaseService<T, ID> {

    protected final JpaRepository<T, ID> repository;

    public BaseService(JpaRepository<T, ID> repository) {
        this.repository = repository;
    }

    // Generic CRUD methods
    public List<T> findAll() {
        return repository.findAll();
    }

    public Optional<T> findById(ID id) {
        return repository.findById(id);
    }

    @Transactional
    public T save(T entity) {
        preSaveHook(entity);
        T savedEntity = repository.save(entity);
        postSaveHook(savedEntity);
        return savedEntity;
    }

    @Transactional
    public T update(ID id, T updatedEntity) {
        T existingEntity = repository.findById(id)
                .orElseThrow(() -> new RuntimeException("Entity not found"));

        updateHook(existingEntity, updatedEntity);
        return repository.save(existingEntity);
    }

    @Transactional
    public void deleteById(ID id) {
        preDeleteHook(id);
        repository.deleteById(id);
        postDeleteHook(id);
    }

    // Hook methods to be overridden if needed
    protected void preSaveHook(T entity) {
        // Default implementation (no-op)
    }

    protected void postSaveHook(T entity) {
        // Default implementation (no-op)
    }

    protected void updateHook(T existingEntity, T updatedEntity) {
        // Default implementation (no-op)
    }

    protected void preDeleteHook(ID id) {
        // Default implementation (no-op)
    }

    protected void postDeleteHook(ID id) {
        // Default implementation (no-op)
    }
}
