package com.example.backend.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.example.backend.model.Document;
import com.example.backend.model.FileEntity;
import com.example.backend.model.FileType;
import com.example.backend.model.Project;
import com.example.backend.repository.DocumentRepo;
import com.example.backend.repository.FileRepo;

@Service
public class DocumentService extends BaseService<Document, Long> {

    private final DocumentRepo documentRepository;
    private final ProjectService projectService;
    private final FileRepo fileRepository;

    public DocumentService(DocumentRepo documentRepository, ProjectService projectService, FileRepo fileRepository) {
        super(documentRepository);
        this.documentRepository = documentRepository;
        this.projectService = projectService;
        this.fileRepository = fileRepository;
    }

    public Document createDocument(Long projectId, Document document) {
        Project project = projectService.findById(projectId)
                .orElseThrow(() -> new RuntimeException("Project not found"));
        document.setProject(project);

        return documentRepository.save(document);
    }

    public Document updateDocument(Long projectId, Long documentId, Document documentDetails) {
        Document document = documentRepository.findById(documentId)
                .orElseThrow(() -> new RuntimeException("Project not found"));

        document.setName(documentDetails.getName());
        document.setContent(documentDetails.getContent());
        document.setVersion(documentRepository
                .findByProjectIdAndName(projectId, document.getName())
                .map(existing -> existing.getVersion() + 1)
                .orElse(1));
        document.setArchived(documentDetails.isArchived());
        document.setType(documentDetails.getType());

        return documentRepository.save(document);
    }

    public Document uploadDocument(Long projectId, MultipartFile file, FileType fileType) {
        Project project = projectService.findById(projectId)
                .orElseThrow(() -> new RuntimeException("Project not found"));

        FileEntity fileEntity = saveFile(file, fileType);

        Document document = new Document();
        document.setName(file.getOriginalFilename());
        document.setType(file.getContentType());
        document.setFilePath(fileEntity.getName());
        document.setProject(project);
        document.setContent("");

        document.setVersion(documentRepository
                .findByProjectIdAndName(projectId, document.getName())
                .map(existing -> existing.getVersion() + 1)
                .orElse(1));

        return documentRepository.save(document);
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        if (!documentRepository.existsById(id)) {
            throw new RuntimeException("Document not found");
        }
        documentRepository.deleteById(id);
    }

    public List<Document> findByProject(Long projectId) {
        return documentRepository.findByProjectId(projectId);
    }

    public void archiveDocument(Long id) {
        Document document = documentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Document not found"));
        document.setArchived(true);
        documentRepository.save(document);
    }

    public void unarchiveDocument(Long id) {
        Document document = documentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Document not found"));
        document.setArchived(false);
        documentRepository.save(document);
    }

    private FileEntity saveFile(MultipartFile file, FileType fileType) {
        try {
            FileEntity fileEntity = new FileEntity();
            fileEntity.setName(file.getOriginalFilename());
            fileEntity.setContentType(file.getContentType());
            fileEntity.setSize(file.getSize());
            fileEntity.setData(file.getBytes());
            fileEntity.setType(fileType);
            return fileRepository.save(fileEntity);
        } catch (IOException e) {
            throw new RuntimeException("Error while saving the file: " + e.getMessage());
        }
    }
}
