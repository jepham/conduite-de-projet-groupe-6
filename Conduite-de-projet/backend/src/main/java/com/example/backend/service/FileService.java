package com.example.backend.service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.example.backend.model.FileEntity;
import com.example.backend.model.FileType;
import com.example.backend.model.Issue;
import com.example.backend.model.Project;
import com.example.backend.repository.FileRepo;
import com.example.backend.repository.IssueRepo;
import com.example.backend.repository.ProjectRepo;

@Service
public class FileService {

    private final FileRepo fileRepository;
    private final ProjectRepo projectRepository;
    private final IssueRepo issueRepository;

    public FileService(FileRepo fileRepository,
            ProjectRepo projectRepository,
            IssueRepo issueRepository) {
        this.fileRepository = fileRepository;
        this.projectRepository = projectRepository;
        this.issueRepository = issueRepository;
    }

    public FileEntity saveFile(MultipartFile file, Long projectId, FileType fileType, String createdAt,
            String customName)
            throws IOException {
        Project project = projectRepository.findById(projectId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid project ID"));

        FileEntity fileEntity = new FileEntity();

        System.out.println("createdAt (ServiceJava) : " + createdAt);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.'SSS'Z'");
        LocalDateTime dateTimeCreatedAt = LocalDateTime.parse(createdAt, formatter);

        fileEntity.setName(customName);
        fileEntity.setContentType(file.getContentType());
        fileEntity.setSize(file.getSize());
        fileEntity.setCreatedAt(dateTimeCreatedAt);
        fileEntity.setData(file.getBytes());
        fileEntity.setType(fileType);
        fileEntity.setProject(project);

        return fileRepository.save(fileEntity);
    }

    @Transactional
    public FileEntity associateFileWithIssue(Long fileId, Long issueId) {
        FileEntity file = fileRepository.findById(fileId)
                .orElseThrow(() -> new RuntimeException("File not found"));

        Issue issue = issueRepository.findById(issueId)
                .orElseThrow(() -> new RuntimeException("Issue not found"));

        if (!issue.getFiles().contains(file)) {
            issue.getFiles().add(file);
            issueRepository.save(issue);
        }

        return file;
    }

    @Transactional
    public void removeFileFromIssue(Long fileId, Long issueId) {
        Issue issue = issueRepository.findById(issueId)
                .orElseThrow(() -> new RuntimeException("Issue not found"));

        issue.getFiles().removeIf(file -> file.getId().equals(fileId));
        issueRepository.save(issue);
    }

    public List<Issue> getIssuesByFile(Long fileId) {
        FileEntity file = fileRepository.findById(fileId)
                .orElseThrow(() -> new RuntimeException("File not found"));

        return issueRepository.findByFiles(file);
    }

    public List<FileEntity> getFilesByIssue(Long issueId) {
        Issue issue = issueRepository.findById(issueId)
                .orElseThrow(() -> new RuntimeException("Issue not found"));

        return issue.getFiles();
    }

    public FileEntity updateFileMetadata(Long fileId, FileEntity updatedFile) {
        FileEntity existingFile = fileRepository.findById(fileId)
                .orElseThrow(() -> new RuntimeException("File not found"));

        // Update specific fields
        if (updatedFile.getName() != null) {
            existingFile.setName(updatedFile.getName());
        }
        if (updatedFile.getType() != null) {
            existingFile.setType(updatedFile.getType());
        }

        return fileRepository.save(existingFile);
    }

    public List<FileEntity> getFilesByType(Long projectId, FileType fileType) {
        return fileRepository.findByProjectIdAndType(projectId, fileType);
    }

    public Optional<FileEntity> getFile(Long fileId) {
        return fileRepository.findById(fileId);
    }

    public void deleteFile(Long fileId) {
        fileRepository.deleteById(fileId);
    }
}
