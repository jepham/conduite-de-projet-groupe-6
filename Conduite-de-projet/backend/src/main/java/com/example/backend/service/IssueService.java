package com.example.backend.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.backend.model.Issue;
import com.example.backend.model.Project;
import com.example.backend.repository.IssueRepo;

@Service
public class IssueService extends BaseService<Issue, Long> {

    private final IssueRepo issueRepo;
    private final ProjectService projectService;

    public IssueService(IssueRepo issueRepo, ProjectService projectService) {
        super(issueRepo);
        this.issueRepo = issueRepo;
        this.projectService = projectService;
    }

    public List<Issue> findByPriority(String priority) {
        return issueRepo.findByPriority(priority);
    }

    public List<Issue> findOpenIssues() {
        return issueRepo.findByStatus("OPEN");
    }

    public List<Issue> getResolvedIssues(Long projectId) {
        return issueRepo.findByProjectIdAndStatus(projectId, "RESOLVED");
    }

    public List<Issue> getUnresolvedIssues(Long projectId) {
        return issueRepo.findByProjectIdAndStatus(projectId, "OPEN");
    }

    public List<Issue> getAllIssues(Long projectId) {
        return issueRepo.findByProjectId(projectId);
    }

    public Issue createIssue(Long projectId, Issue issue) {
        Project project = projectService.findById(projectId)
                .orElseThrow(() -> new RuntimeException("Project not found"));
        issue.setProject(project);
        return issueRepo.save(issue);
    }

    public Issue update(Long id, Issue issue) {
        Issue existingIssue = issueRepo.findById(id)
                .orElseThrow(() -> new RuntimeException("Project not found"));
        existingIssue.setTitle(issue.getTitle());
        existingIssue.setDescription(issue.getDescription());
        existingIssue.setPriority(issue.getPriority());
        existingIssue.setStatus(issue.getStatus());
        return issueRepo.save(existingIssue);
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        if (!issueRepo.existsById(id)) {
            throw new RuntimeException("Issue not found");
        }
        issueRepo.deleteById(id);
    }

}
