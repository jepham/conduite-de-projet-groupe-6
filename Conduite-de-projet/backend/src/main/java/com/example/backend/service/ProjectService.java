package com.example.backend.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.backend.model.Project;
import com.example.backend.model.ProjectStatus;
import com.example.backend.model.User;
import com.example.backend.repository.ProjectRepo;

@Service
public class ProjectService extends BaseService<Project, Long> {

    private final ProjectRepo projectRepo;

    public ProjectService(ProjectRepo projectRepo) {
        super(projectRepo);
        this.projectRepo = projectRepo;
    }

    public Project createProject(User user, Project project) {
        user.addProject(project);
        return projectRepo.save(project);
    }

    @Override
    protected void preDeleteHook(Long projectId) {
        Project project = repository.findById(projectId)
                .orElseThrow(() -> new RuntimeException("Project not found"));
        if (!project.getTasks().isEmpty()) {
            throw new RuntimeException("Cannot delete a project with active tasks");
        }
    }

    public List<Project> findByStatus(ProjectStatus status) {
        return projectRepo.findByStatus(status);
    }

    public List<Project> findByUserId(Long userid) {
        return projectRepo.findByUserId(userid);
    }

    public void archiveProject(Long projectId) {
        Project project = repository.findById(projectId)
                .orElseThrow(() -> new RuntimeException("Project not found"));
        project.setStatus(ProjectStatus.COMPLETED);
        save(project);
    }

    public Project updateProject(Long id, Project projectDetails) {
        Project project = projectRepo.findById(id)
                .orElseThrow(() -> new RuntimeException("Project not found"));

        project.setName(projectDetails.getName());
        project.setDescription(projectDetails.getDescription());
        project.setStatus(projectDetails.getStatus());

        return projectRepo.save(project);
    }

    public List<Project> getAllProjects() {
        return projectRepo.findAll();
    }

    public Project findProject(Long projectId) {
        return projectRepo.getById(projectId);
    }

    public void deleteProject(Long id) {
        projectRepo.deleteById(id);
    }
}
