package com.example.backend.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.backend.model.Project;
import com.example.backend.model.Task;
import com.example.backend.model.TaskDTO;
import com.example.backend.model.TaskStatus;
import com.example.backend.model.User;
import com.example.backend.repository.ProjectRepo;
import com.example.backend.repository.TaskRepo;
import com.example.backend.repository.UserRepo;

@Service
public class TaskService extends BaseService<Task, Long> {

    private final TaskRepo taskRepo;
    private final UserRepo userRepo;
    private final ProjectRepo projectRepo;

    public TaskService(TaskRepo taskRepo, UserRepo userRepo, ProjectRepo projectRepo) {
        super(taskRepo);
        this.taskRepo = taskRepo;
        this.userRepo = userRepo;
        this.projectRepo = projectRepo;
    }

    @Transactional
    public Task createTask(Long projectId, TaskDTO taskDTO) {
        Project project = projectRepo.findById(projectId)
                .orElseThrow(() -> new RuntimeException("Project not found"));

        Task task = taskDTO.toEntity();
        task.setProject(project);

        // Assign user if provided
        if (taskDTO.getUserId() != null) {
            User user = userRepo.findById(taskDTO.getUserId())
                    .orElseThrow(() -> new RuntimeException("User not found"));
            user.addDevelopingTask(task);
            task.setUserDeveloping(user);
        }

        return taskRepo.save(task);
    }

    @Transactional
    public Task update(Long taskId, TaskDTO updatedTaskDataDTO) {
        Task existingTask = taskRepo.findById(taskId)
                .orElseThrow(() -> new RuntimeException("Task not found"));

        // Update modifiable fields
        if (updatedTaskDataDTO.getTitle() != null) {
            existingTask.setTitle(updatedTaskDataDTO.getTitle());
        }
        if (updatedTaskDataDTO.getDescription() != null) {
            existingTask.setDescription(updatedTaskDataDTO.getDescription());
        }
        existingTask.setCompleted(updatedTaskDataDTO.isCompleted());

        // Update status based on completion
        existingTask.setStatus(updatedTaskDataDTO.isCompleted() ? TaskStatus.COMPLETED : TaskStatus.IN_PROGRESS);

        if (updatedTaskDataDTO.getDeadline() != null) {
            existingTask.setDeadline(updatedTaskDataDTO.getDeadline());
        }

        // Assign user if provided
        if (updatedTaskDataDTO.getUserId() != null) {
            User newUser = userRepo.findById(updatedTaskDataDTO.getUserId())
                    .orElseThrow(() -> new RuntimeException("User not found"));

            // Remove task from old user if exists
            if (existingTask.getUserDeveloping() != null) {
                existingTask.getUserDeveloping().removeDevelopingTask(existingTask);
            }

            // Assign task to new user
            newUser.addDevelopingTask(existingTask);
            existingTask.setUserDeveloping(newUser);
        } else {
            // Remove user assignment if not provided
            if (existingTask.getUserDeveloping() != null) {
                existingTask.getUserDeveloping().removeDevelopingTask(existingTask);
            }
        }

        return taskRepo.save(existingTask);
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        if (!taskRepo.existsById(id)) {
            throw new RuntimeException("Task not found");
        }
        taskRepo.deleteById(id);
    }

    public List<Task> getCompletedTasks(Long projectId) {
        return taskRepo.findByProjectIdAndStatus(projectId, TaskStatus.COMPLETED);
    }

    public List<Task> getPendingTasks(Long projectId) {
        return taskRepo.findByProjectIdAndStatus(projectId, TaskStatus.PENDING);
    }

    public List<Task> getAllTasks(Long projectId) {
        return taskRepo.findByProjectId(projectId);
    }

    public List<Task> getInProgressTasks(Long projectId) {
        return taskRepo.findByProjectIdAndStatus(projectId, TaskStatus.IN_PROGRESS);
    }
}
