package com.example.backend.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.backend.model.Project;
import com.example.backend.model.Test;
import com.example.backend.repository.TestRepo;

@Service
public class TestService extends BaseService<Test, Long> {

    private final TestRepo testRepo;
    private final ProjectService projectService;

    public TestService(TestRepo testRepo, ProjectService projectService) {
        super(testRepo);
        this.testRepo = testRepo;
        this.projectService = projectService;
    }

    public Test createTest(Long projectId, Test test) {
        Project project = projectService.findById(projectId)
                .orElseThrow(() -> new RuntimeException("Project not found"));
        test.setProject(project);
        return testRepo.save(test);
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        if (!testRepo.existsById(id)) {
            throw new RuntimeException("Test not found");
        }
        testRepo.deleteById(id);
    }

    public List<Test> getAllTests(Long projectId) {
        return testRepo.findByProjectId(projectId);
    }
}
