package com.example.backend.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.backend.model.User;
import com.example.backend.service.AuthentificationService;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class AuthentificationControllerTest {

    @Mock
    private AuthentificationService authentificationService;

    @InjectMocks
    private AuthentificationController authentificationController;

    private User mockUser;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        mockUser = new User();
        mockUser.setUsername("testUser");
        mockUser.setEmail("test@example.com");
    }

    @Test
    void testLoginSuccess() {
        when(authentificationService.authenticate("testUser", "password")).thenReturn(true);
        when(authentificationService.getUserByUsername("testUser")).thenReturn(mockUser);
        when(authentificationService.generateToken(mockUser)).thenReturn("mockToken");

        Map<String, Object> response = authentificationController.login("testUser", "password");

        assertNotNull(response);
        assertEquals("mockToken", response.get("token"));
        assertEquals("testUser", response.get("username"));
    }

    @Test
    void testLoginFailure() {
        when(authentificationService.authenticate("testUser", "wrongPassword")).thenReturn(false);

        Exception exception = assertThrows(RuntimeException.class, () ->
                authentificationController.login("testUser", "wrongPassword")
        );

        assertEquals("Invalid credentials", exception.getMessage());
    }

    @Test
    void testRegister() {
        when(authentificationService.register(mockUser)).thenReturn(mockUser);

        User response = authentificationController.register(mockUser);

        assertNotNull(response);
        assertEquals("testUser", response.getUsername());
        verify(authentificationService, times(1)).register(mockUser);
    }
}
