package com.example.backend.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

import com.example.backend.model.Document;
import com.example.backend.model.FileType;
import com.example.backend.service.DocumentService;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;

class DocumentControllerTest {

    @Mock
    private DocumentService documentService;

    @InjectMocks
    private DocumentController documentController;

    private Document mockDocument;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        // Initialize mock data
        mockDocument = new Document();
        mockDocument.setId(1L);
        mockDocument.setName("Test Document");
        mockDocument.setType("text/plain");
        mockDocument.setContent("Sample content");
    }

    @Test
    void testGetDocumentsByProjectSuccess() {
        when(documentService.findByProject(1L)).thenReturn(List.of(mockDocument));

        ResponseEntity<List<Document>> response = documentController.getDocumentsByProject(1L);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().size());
        assertEquals("Test Document", response.getBody().get(0).getName());
    }

    @Test
    void testCreateDocumentSuccess() {
        when(documentService.createDocument(eq(1L), any(Document.class))).thenReturn(mockDocument);

        ResponseEntity<Document> response = documentController.createDocument(1L, mockDocument);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("Test Document", response.getBody().getName());
    }

    @Test
    void testCreateDocumentFailure() {
        when(documentService.createDocument(eq(1L), any(Document.class))).thenThrow(new RuntimeException("Creation error"));

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                documentController.createDocument(1L, mockDocument));

        assertEquals("Creation error", exception.getMessage());
    }

    @Test
    void testUploadDocumentSuccess() {
        MockMultipartFile mockFile = new MockMultipartFile(
                "file",
                "test-document.txt",
                "text/plain",
                "Sample content".getBytes()
        );

        when(documentService.uploadDocument(eq(1L), eq(mockFile), eq(FileType.DOCUMENT))).thenReturn(mockDocument);

        ResponseEntity<Document> response = documentController.uploadDocument(1L, mockFile, FileType.DOCUMENT);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("Test Document", response.getBody().getName());
    }

    @Test
    void testUpdateDocumentSuccess() {
        Document updatedDocument = new Document();
        updatedDocument.setName("Updated Document");
        updatedDocument.setContent("Updated Content");

        when(documentService.updateDocument(eq(1L), eq(1L), any(Document.class))).thenReturn(updatedDocument);

        ResponseEntity<Document> response = documentController.updateDocument(1L, 1L, updatedDocument);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("Updated Document", response.getBody().getName());
    }

    @Test
    void testArchiveDocumentSuccess() {
        doNothing().when(documentService).archiveDocument(1L);

        ResponseEntity<Void> response = documentController.archiveDocument(1L);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
    }

    @Test
    void testDeleteDocumentSuccess() {
        doNothing().when(documentService).deleteById(1L);

        ResponseEntity<Void> response = documentController.deleteDocument(1L);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
    }

    @Test
    void testHandleRuntimeException() {
        ResponseEntity<String> response = documentController.handleRuntimeException(new RuntimeException("Error occurred"));

        assertNotNull(response);
        assertEquals(BAD_REQUEST, response.getStatusCode());
        assertEquals("Error occurred", response.getBody());
    }
}
