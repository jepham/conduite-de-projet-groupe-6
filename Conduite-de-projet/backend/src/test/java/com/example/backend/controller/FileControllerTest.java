package com.example.backend.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

import com.example.backend.model.FileEntity;
import com.example.backend.model.FileType;
import com.example.backend.model.Issue;
import com.example.backend.service.FileService;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;

class FileControllerTest {

    @Mock
    private FileService fileService;

    @InjectMocks
    private FileController fileController;

    private FileEntity mockFileEntity;
    private Issue mockIssue;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        // Initialize mock data
        mockFileEntity = new FileEntity();
        mockFileEntity.setId(1L);
        mockFileEntity.setName("test-file.txt");
        mockFileEntity.setType(FileType.DOCUMENT);
        mockFileEntity.setContentType("text/plain");
        mockFileEntity.setData("Sample content".getBytes());

        mockIssue = new Issue();
        mockIssue.setId(1L);
    }

    @Test
    void testUploadFileSuccess() throws IOException {
        MockMultipartFile mockMultipartFile = new MockMultipartFile(
                "file",
                "test-file.txt",
                "text/plain",
                "Sample content".getBytes()
        );

        when(fileService.saveFile(eq(mockMultipartFile), eq(1L), eq(FileType.DOCUMENT), eq("2023-01-01"), eq("customName")))
                .thenReturn(mockFileEntity);

        ResponseEntity<String> response = fileController.uploadFile(1L, FileType.DOCUMENT, mockMultipartFile, "customName", "2023-01-01");

        assertNotNull(response);
        assertEquals(CREATED, response.getStatusCode());
        assertTrue(response.getBody().contains("File uploaded successfully! ID:"));
    }

    @Test
    void testUploadFileFailure() throws IOException {
        MockMultipartFile mockMultipartFile = new MockMultipartFile(
                "file",
                "test-file.txt",
                "text/plain",
                "Sample content".getBytes()
        );

        when(fileService.saveFile(eq(mockMultipartFile), eq(1L), eq(FileType.DOCUMENT), eq("2023-01-01"), eq("customName")))
                .thenThrow(new RuntimeException("File upload failed"));

        ResponseEntity<String> response = fileController.uploadFile(1L, FileType.DOCUMENT, mockMultipartFile, "customName", "2023-01-01");

        assertNotNull(response);
        assertEquals(INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertTrue(response.getBody().contains("File upload failed"));
    }

    @Test
    void testGetFilesByTypeSuccess() {
        when(fileService.getFilesByType(1L, FileType.DOCUMENT)).thenReturn(List.of(mockFileEntity));

        ResponseEntity<List<FileEntity>> response = fileController.getFilesByType(1L, FileType.DOCUMENT);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().size());
        assertEquals("test-file.txt", response.getBody().get(0).getName());
    }

    @Test
    void testGetFileSuccess() {
        when(fileService.getFile(1L)).thenReturn(Optional.of(mockFileEntity));

        ResponseEntity<?> response = fileController.getFile(1L, FileType.DOCUMENT, 1L);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertTrue(response.getHeaders().containsKey(HttpHeaders.CONTENT_DISPOSITION));
        assertEquals(mockFileEntity.getData(), response.getBody());
    }

    @Test
    void testGetFileNotFound() {
        when(fileService.getFile(1L)).thenReturn(Optional.empty());

        ResponseEntity<?> response = fileController.getFile(1L, FileType.DOCUMENT, 1L);

        assertNotNull(response);
        assertEquals(NOT_FOUND, response.getStatusCode());
    }

    @Test
    void testDeleteFileSuccess() {
        when(fileService.getFile(1L)).thenReturn(Optional.of(mockFileEntity));
        doNothing().when(fileService).deleteFile(1L);

        ResponseEntity<?> response = fileController.deleteFile(1L, FileType.DOCUMENT, 1L);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
    }

    @Test
    void testDeleteFileFailure() {
        doThrow(new RuntimeException("Delete error")).when(fileService).deleteFile(1L);

        ResponseEntity<?> response = fileController.deleteFile(1L, FileType.DOCUMENT, 1L);

        assertNotNull(response);
        assertEquals(BAD_REQUEST, response.getStatusCode());
        assertTrue(response.getBody().toString().contains("File type mismatch or file not found."));
    }

    @Test
    void testAssociateFileWithIssueSuccess() {
        when(fileService.associateFileWithIssue(1L, 1L)).thenReturn(mockFileEntity);

        ResponseEntity<FileEntity> response = fileController.associateFileWithIssue(1L, 1L);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals(mockFileEntity, response.getBody());
    }

    @Test
    void testRemoveFileFromIssueSuccess() {
        doNothing().when(fileService).removeFileFromIssue(1L, 1L);

        ResponseEntity<?> response = fileController.removeFileFromIssue(1L, 1L);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
    }

    @Test
    void testUpdateFileMetadataSuccess() {
        FileEntity updatedFile = new FileEntity();
        updatedFile.setName("updated-file.txt");

        when(fileService.updateFileMetadata(1L, updatedFile)).thenReturn(updatedFile);

        ResponseEntity<FileEntity> response = fileController.updateFileMetadata(1L, updatedFile);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals("updated-file.txt", response.getBody().getName());
    }
}
