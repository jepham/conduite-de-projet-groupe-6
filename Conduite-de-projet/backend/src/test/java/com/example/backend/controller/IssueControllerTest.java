package com.example.backend.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.OK;

import com.example.backend.model.Issue;
import com.example.backend.service.IssueService;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

class IssueControllerTest {

    @Mock
    private IssueService issueService;

    @InjectMocks
    private IssueController issueController;

    private Issue mockIssue;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        mockIssue = new Issue();
        mockIssue.setId(1L);
        mockIssue.setTitle("Sample Issue");
        mockIssue.setPriority("High");
        mockIssue.setStatus("Open");
    }

    @Test
    void testGetIssuesByPriority() {
        when(issueService.findByPriority("High")).thenReturn(List.of(mockIssue));

        List<Issue> response = issueController.getIssuesByPriority("High");

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals("High", response.get(0).getPriority());
        verify(issueService, times(1)).findByPriority("High");
    }

    @Test
    void testCreateIssueSuccess() {
        when(issueService.createIssue(eq(1L), any(Issue.class))).thenReturn(mockIssue);

        Issue response = issueController.createIssue(1L, mockIssue);

        assertNotNull(response);
        assertEquals("Sample Issue", response.getTitle());
        verify(issueService, times(1)).createIssue(eq(1L), any(Issue.class));
    }

    @Test
    void testCreateIssueFailure() {
        when(issueService.createIssue(eq(1L), any(Issue.class))).thenThrow(new RuntimeException("Creation failed"));

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                issueController.createIssue(1L, mockIssue));

        assertEquals("Creation failed", exception.getMessage());
        verify(issueService, times(1)).createIssue(eq(1L), any(Issue.class));
    }

    @Test
    void testUpdateIssueSuccess() {
        Issue updatedIssue = new Issue();
        updatedIssue.setTitle("Updated Issue");

        when(issueService.update(eq(1L), any(Issue.class))).thenReturn(updatedIssue);

        ResponseEntity<Issue> response = issueController.updateIssue(1L, 1L, updatedIssue);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals("Updated Issue", response.getBody().getTitle());
        verify(issueService, times(1)).update(eq(1L), any(Issue.class));
    }

    @Test
    void testUpdateIssueFailure() {
        when(issueService.update(eq(1L), any(Issue.class))).thenThrow(new RuntimeException("Update failed"));

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                issueController.updateIssue(1L, 1L, mockIssue));

        assertEquals("Update failed", exception.getMessage());
        verify(issueService, times(1)).update(eq(1L), any(Issue.class));
    }

    @Test
    void testDeleteIssueSuccess() {
        doNothing().when(issueService).deleteById(1L);

        ResponseEntity<?> response = issueController.deleteIssue(1L, 1L);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        verify(issueService, times(1)).deleteById(1L);
    }

    @Test
    void testDeleteIssueFailure() {
        doThrow(new RuntimeException("Delete failed")).when(issueService).deleteById(1L);

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                issueController.deleteIssue(1L, 1L));

        assertEquals("Delete failed", exception.getMessage());
        verify(issueService, times(1)).deleteById(1L);
    }

    @Test
    void testGetAllIssues() {
        when(issueService.getAllIssues(1L)).thenReturn(List.of(mockIssue));

        List<Issue> response = issueController.getAllIssues(1L);

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals("Sample Issue", response.get(0).getTitle());
        verify(issueService, times(1)).getAllIssues(1L);
    }

    @Test
    void testGetOpenIssues() {
        when(issueService.findOpenIssues()).thenReturn(List.of(mockIssue));

        List<Issue> response = issueController.getOpenIssues();

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals("Open", response.get(0).getStatus());
        verify(issueService, times(1)).findOpenIssues();
    }
}
