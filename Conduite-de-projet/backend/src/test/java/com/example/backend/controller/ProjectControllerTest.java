package com.example.backend.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.OK;

import com.example.backend.model.Project;
import com.example.backend.model.ProjectStatus;
import com.example.backend.model.User;
import com.example.backend.service.ProjectService;
import com.example.backend.service.UserService;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

class ProjectControllerTest {

    @Mock
    private ProjectService projectService;

    @Mock
    private UserService userService;

    @InjectMocks
    private ProjectController projectController;

    private Project mockProject;
    private User mockUser;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        mockProject = new Project();
        mockProject.setId(1L);
        mockProject.setName("Test Project");
        mockProject.setStatus(ProjectStatus.IN_PROGRESS);

        mockUser = new User();
        mockUser.setId(1L);
        mockUser.setUsername("testUser");
    }

    @Test
    void testGetAllProjects() {
        when(projectService.getAllProjects()).thenReturn(List.of(mockProject));

        List<Project> response = projectController.getAllProjects();

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals("Test Project", response.get(0).getName());
        verify(projectService, times(1)).getAllProjects();
    }

    @Test
    void testCreateProjectSuccess() {
        when(userService.getUserByUsername("testUser")).thenReturn(mockUser);
        when(projectService.createProject(mockUser, mockProject)).thenReturn(mockProject);

        Project response = projectController.createProject("testUser", mockProject);

        assertNotNull(response);
        assertEquals("Test Project", response.getName());
        verify(userService, times(1)).getUserByUsername("testUser");
        verify(projectService, times(1)).createProject(mockUser, mockProject);
    }

    @Test
    void testCreateProjectUserNotFound() {
        when(userService.getUserByUsername("nonExistentUser")).thenThrow(new RuntimeException("User not found"));

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                projectController.createProject("nonExistentUser", mockProject));

        assertEquals("User not found", exception.getMessage());
        verify(userService, times(1)).getUserByUsername("nonExistentUser");
        verify(projectService, never()).createProject(any(), any());
    }

    @Test
    void testGetProjectsByIDSuccess() {
        when(userService.getUserByUsername("testUser")).thenReturn(mockUser);
        when(projectService.findByUserId(1L)).thenReturn(List.of(mockProject));

        List<Project> response = projectController.getProjectsByID("testUser");

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals("Test Project", response.get(0).getName());
        verify(userService, times(1)).getUserByUsername("testUser");
        verify(projectService, times(1)).findByUserId(1L);
    }

    @Test
    void testGetProjectsByStatus() {
        when(projectService.findByStatus(ProjectStatus.IN_PROGRESS)).thenReturn(List.of(mockProject));

        List<Project> response = projectController.getProjectsByStatus(ProjectStatus.IN_PROGRESS);

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals(ProjectStatus.IN_PROGRESS, response.get(0).getStatus());
        verify(projectService, times(1)).findByStatus(ProjectStatus.IN_PROGRESS);
    }

    @Test
    void testArchiveProjectSuccess() {
        doNothing().when(projectService).archiveProject(1L);

        ResponseEntity<?> response = projectController.archiveProject(1L);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        verify(projectService, times(1)).archiveProject(1L);
    }

    @Test
    void testUpdateProjectSuccess() {
        Project updatedProject = new Project();
        updatedProject.setName("Updated Project");

        when(projectService.updateProject(1L, updatedProject)).thenReturn(updatedProject);

        ResponseEntity<Project> response = projectController.updateProject(1L, updatedProject);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals("Updated Project", response.getBody().getName());
        verify(projectService, times(1)).updateProject(1L, updatedProject);
    }

    @Test
    void testDeleteProjectSuccess() {
        doNothing().when(projectService).deleteProject(1L);

        ResponseEntity<?> response = projectController.deleteProject(1L);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        verify(projectService, times(1)).deleteProject(1L);
    }

    @Test
    void testDeleteProjectFailure() {
        doThrow(new RuntimeException("Delete error")).when(projectService).deleteProject(1L);

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                projectController.deleteProject(1L));

        assertEquals("Delete error", exception.getMessage());
        verify(projectService, times(1)).deleteProject(1L);
    }
}
