package com.example.backend.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.OK;

import com.example.backend.model.Task;
import com.example.backend.model.TaskDTO;
import com.example.backend.service.TaskService;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

class TaskControllerTest {

    @Mock
    private TaskService taskService;

    @InjectMocks
    private TaskController taskController;

    private Task mockTask;
    private TaskDTO mockTaskDTO;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        // Initialize mock data
        mockTask = new Task();
        mockTask.setId(1L);
        mockTask.setTitle("Test Task");

        mockTaskDTO = new TaskDTO();
        mockTaskDTO.setTitle("New Task");
    }

    @Test
    void testGetTasksByProjectSuccess() {
        when(taskService.getAllTasks(1L)).thenReturn(List.of(mockTask));

        List<Task> response = taskController.getTasksByProject(1L);

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals("Test Task", response.get(0).getTitle());
        verify(taskService, times(1)).getAllTasks(1L);
    }

    @Test
    void testCreateTaskSuccess() {
        when(taskService.createTask(1L, mockTaskDTO)).thenReturn(mockTask);

        Task response = taskController.createTask(1L, mockTaskDTO);

        assertNotNull(response);
        assertEquals("Test Task", response.getTitle());
        verify(taskService, times(1)).createTask(1L, mockTaskDTO);
    }

    @Test
    void testCreateTaskFailure() {
        when(taskService.createTask(1L, mockTaskDTO)).thenThrow(new RuntimeException("Creation error"));

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                taskController.createTask(1L, mockTaskDTO));

        assertEquals("Creation error", exception.getMessage());
        verify(taskService, times(1)).createTask(1L, mockTaskDTO);
    }

    @Test
    void testUpdateTaskSuccess() {
        TaskDTO updatedTaskDTO = new TaskDTO();
        updatedTaskDTO.setTitle("Updated Task");

        when(taskService.update(1L, updatedTaskDTO)).thenReturn(mockTask);

        ResponseEntity<Task> response = taskController.updateTask(1L, 1L, updatedTaskDTO);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals("Test Task", response.getBody().getTitle());
        verify(taskService, times(1)).update(1L, updatedTaskDTO);
    }

    @Test
    void testUpdateTaskFailure() {
        TaskDTO updatedTaskDTO = new TaskDTO();
        updatedTaskDTO.setTitle("Updated Task");

        when(taskService.update(1L, updatedTaskDTO)).thenThrow(new RuntimeException("Update error"));

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                taskController.updateTask(1L, 1L, updatedTaskDTO));

        assertEquals("Update error", exception.getMessage());
        verify(taskService, times(1)).update(1L, updatedTaskDTO);
    }

    @Test
    void testDeleteTaskSuccess() {
        doNothing().when(taskService).deleteById(1L);

        ResponseEntity<?> response = taskController.deleteTask(1L, 1L);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        verify(taskService, times(1)).deleteById(1L);
    }

    @Test
    void testDeleteTaskFailure() {
        doThrow(new RuntimeException("Delete error")).when(taskService).deleteById(1L);

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                taskController.deleteTask(1L, 1L));

        assertEquals("Delete error", exception.getMessage());
        verify(taskService, times(1)).deleteById(1L);
    }

    @Test
    void testGetCompletedTasksSuccess() {
        when(taskService.getCompletedTasks(1L)).thenReturn(List.of(mockTask));

        List<Task> response = taskController.getCompletedTasks(1L);

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals("Test Task", response.get(0).getTitle());
        verify(taskService, times(1)).getCompletedTasks(1L);
    }

    @Test
    void testGetPendingTasksSuccess() {
        when(taskService.getPendingTasks(1L)).thenReturn(List.of(mockTask));

        List<Task> response = taskController.getPendingTasks(1L);

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals("Test Task", response.get(0).getTitle());
        verify(taskService, times(1)).getPendingTasks(1L);
    }

    @Test
    void testGetInProgressTasksSuccess() {
        when(taskService.getInProgressTasks(1L)).thenReturn(List.of(mockTask));

        List<Task> response = taskController.getInProgressTasks(1L);

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals("Test Task", response.get(0).getTitle());
        verify(taskService, times(1)).getInProgressTasks(1L);
    }
}
