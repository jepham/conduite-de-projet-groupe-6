package com.example.backend.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

import com.example.backend.service.TestService;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

class TestControllerTest {

    @Mock
    private TestService testService;

    @InjectMocks
    private TestController testController;

    private com.example.backend.model.Test mockTest;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        mockTest = new com.example.backend.model.Test();
        mockTest.setId(1L);
        mockTest.setTitle("Sample Test");
    }

    @Test
    void testCreateTestSuccess() {
        when(testService.createTest(eq(1L), any(com.example.backend.model.Test.class))).thenReturn(mockTest);

        ResponseEntity<com.example.backend.model.Test> response = testController.createTest(1L, mockTest);

        assertNotNull(response);
        assertEquals(CREATED, response.getStatusCode());
        assertEquals("Sample Test", Objects.requireNonNull(response.getBody()).getTitle());
        verify(testService, times(1)).createTest(eq(1L), any(com.example.backend.model.Test.class));
    }

    @Test
    void testCreateTestFailure() {
        when(testService.createTest(eq(1L), any(com.example.backend.model.Test.class))).thenThrow(new RuntimeException("Creation failed"));

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                testController.createTest(1L, mockTest));

        assertEquals("Creation failed", exception.getMessage());
        verify(testService, times(1)).createTest(eq(1L), any(com.example.backend.model.Test.class));
    }

    @Test
    void testGetAllTestsSuccess() {
        when(testService.getAllTests(1L)).thenReturn(List.of(mockTest));

        ResponseEntity<List<com.example.backend.model.Test>> response = testController.getAllTests(1L);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals(1, response.getBody().size());
        assertEquals("Sample Test", response.getBody().get(0).getTitle());
        verify(testService, times(1)).getAllTests(1L);
    }

    @Test
    void testGetAllTestsFailure() {
        when(testService.getAllTests(1L)).thenThrow(new RuntimeException("Retrieval failed"));

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                testController.getAllTests(1L));

        assertEquals("Retrieval failed", exception.getMessage());
        verify(testService, times(1)).getAllTests(1L);
    }

    @Test
    void testDeleteTestSuccess() {
        doNothing().when(testService).deleteById(1L);

        ResponseEntity<Void> response = testController.deleteTest(1L);

        assertNotNull(response);
        assertEquals(NO_CONTENT, response.getStatusCode());
        verify(testService, times(1)).deleteById(1L);
    }

    @Test
    void testDeleteTestFailure() {
        doThrow(new RuntimeException("Delete error")).when(testService).deleteById(1L);

        ResponseEntity<Void> response = testController.deleteTest(1L);

        assertEquals(NOT_FOUND, response.getStatusCode());
        verify(testService, times(1)).deleteById(1L);
    }
}
