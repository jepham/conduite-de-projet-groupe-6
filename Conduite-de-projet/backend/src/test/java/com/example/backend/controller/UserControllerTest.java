package com.example.backend.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

import com.example.backend.model.User;
import com.example.backend.service.UserService;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

class UserControllerTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    private User mockUser;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        // Initialize mock data
        mockUser = new User();
        mockUser.setId(1L);
        mockUser.setUsername("testUser");
        mockUser.setEmail("test@example.com");
    }

    @Test
    void testGetUserByUsernameSuccess() {
        when(userService.getUserByUsername("testUser")).thenReturn(mockUser);

        ResponseEntity<User> response = userController.getUserByUsername("testUser");

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("testUser", response.getBody().getUsername());
    }

    @Test
    void testGetUserByUsernameNotFound() {
        when(userService.getUserByUsername("nonExistentUser")).thenThrow(new RuntimeException("User not found"));

        ResponseEntity<User> response = userController.getUserByUsername("nonExistentUser");

        assertNotNull(response);
        assertEquals(NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    void testGetUserByIdSuccess() {
        when(userService.getUserById(1L)).thenReturn(mockUser);

        ResponseEntity<User> response = userController.getUserById(1L);

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1L, response.getBody().getId());
    }

    @Test
    void testGetUserByIdNotFound() {
        when(userService.getUserById(1L)).thenThrow(new RuntimeException("User not found"));

        ResponseEntity<User> response = userController.getUserById(1L);

        assertNotNull(response);
        assertEquals(NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    void testGetUserByEmailSuccess() {
        when(userService.getUserByEmail("test@example.com")).thenReturn(mockUser);

        ResponseEntity<User> response = userController.getUserByEmail("test@example.com");

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("test@example.com", response.getBody().getEmail());
    }

    @Test
    void testGetUserByEmailNotFound() {
        when(userService.getUserByEmail("nonExistent@example.com")).thenThrow(new RuntimeException("User not found"));

        ResponseEntity<User> response = userController.getUserByEmail("nonExistent@example.com");

        assertNotNull(response);
        assertEquals(NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    void testGetAllUsersSuccess() {
        when(userService.getAllUsers()).thenReturn(List.of(mockUser));

        ResponseEntity<List<User>> response = userController.getAllUsers();

        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().size());
        assertEquals("testUser", response.getBody().get(0).getUsername());
    }

    @Test
    void testGetAllUsersFailure() {
        when(userService.getAllUsers()).thenThrow(new RuntimeException("Internal server error"));

        ResponseEntity<List<User>> response = userController.getAllUsers();

        assertNotNull(response);
        assertEquals(INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertNull(response.getBody());
    }
}
