package com.example.backend.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.backend.model.User;
import com.example.backend.repository.AuthentificationRepo;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

class AuthentificationServiceTest {

    @Mock
    private AuthentificationRepo authentificationRepo;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private AuthentificationService authentificationService;

    private User mockUser;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        mockUser = new User();
        mockUser.setUsername("testUser");
        mockUser.setPassword("hashedPassword");
        mockUser.setEmail("test@example.com");
    }

    @Test
    void testAuthenticateSuccess() {
        when(authentificationRepo.findByUsername("testUser")).thenReturn(Optional.of(mockUser));
        when(passwordEncoder.matches("plainPassword", "hashedPassword")).thenReturn(true);

        boolean result = authentificationService.authenticate("testUser", "plainPassword");

        assertTrue(result);
        verify(authentificationRepo, times(1)).findByUsername("testUser");
        verify(passwordEncoder, times(1)).matches("plainPassword", "hashedPassword");
    }

    @Test
    void testAuthenticateFailureWrongPassword() {
        when(authentificationRepo.findByUsername("testUser")).thenReturn(Optional.of(mockUser));
        when(passwordEncoder.matches("wrongPassword", "hashedPassword")).thenReturn(false);

        boolean result = authentificationService.authenticate("testUser", "wrongPassword");

        assertFalse(result);
        verify(passwordEncoder, times(1)).matches("wrongPassword", "hashedPassword");
    }

    @Test
    void testAuthenticateUserNotFound() {
        when(authentificationRepo.findByUsername("nonExistentUser")).thenReturn(Optional.empty());

        boolean result = authentificationService.authenticate("nonExistentUser", "anyPassword");

        assertFalse(result);
        verify(authentificationRepo, times(1)).findByUsername("nonExistentUser");
    }

    @Test
    void testGenerateToken() {
        String token = authentificationService.generateToken(mockUser);

        assertNotNull(token);
    }

    @Test
    void testRegisterSuccess() {
        when(authentificationRepo.findByUsername("testUser")).thenReturn(Optional.empty());
        when(authentificationRepo.findByEmail("test@example.com")).thenReturn(Optional.empty());
        when(passwordEncoder.encode("plainPassword")).thenReturn("hashedPassword");
        when(authentificationRepo.save(mockUser)).thenReturn(mockUser);

        mockUser.setPassword("plainPassword");
        User result = authentificationService.register(mockUser);

        assertNotNull(result);
        assertEquals("hashedPassword", result.getPassword());
        verify(authentificationRepo, times(1)).save(mockUser);
    }

    @Test
    void testRegisterUsernameAlreadyTaken() {
        when(authentificationRepo.findByUsername("testUser")).thenReturn(Optional.of(mockUser));

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                authentificationService.register(mockUser));

        assertEquals("Username already taken", exception.getMessage());
        verify(authentificationRepo, never()).save(any(User.class));
    }

    @Test
    void testRegisterEmailAlreadyTaken() {
        when(authentificationRepo.findByUsername("testUser")).thenReturn(Optional.empty());
        when(authentificationRepo.findByEmail("test@example.com")).thenReturn(Optional.of(mockUser));

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                authentificationService.register(mockUser));

        assertEquals("Email already taken", exception.getMessage());
        verify(authentificationRepo, never()).save(any(User.class));
    }

    @Test
    void testGetUserByUsernameSuccess() {
        when(authentificationRepo.findByUsername("testUser")).thenReturn(Optional.of(mockUser));

        User result = authentificationService.getUserByUsername("testUser");

        assertNotNull(result);
        assertEquals("testUser", result.getUsername());
        verify(authentificationRepo, times(1)).findByUsername("testUser");
    }

    @Test
    void testGetUserByUsernameNotFound() {
        when(authentificationRepo.findByUsername("nonExistentUser")).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                authentificationService.getUserByUsername("nonExistentUser"));

        assertEquals("User not found with username: nonExistentUser", exception.getMessage());
        verify(authentificationRepo, times(1)).findByUsername("nonExistentUser");
    }
}
