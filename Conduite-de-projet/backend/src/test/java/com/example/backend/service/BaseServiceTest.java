package com.example.backend.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.jpa.repository.JpaRepository;

class BaseServiceTest {

    // Mock repository and create a concrete subclass of BaseService for testing
    @Mock
    private JpaRepository<TestEntity, Long> mockRepository;

    @InjectMocks
    private TestEntityService testEntityService;

    private TestEntity mockEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        mockEntity = new TestEntity(1L, "Test Name");
    }

    @Test
    void testFindAll() {
        when(mockRepository.findAll()).thenReturn(List.of(mockEntity));

        List<TestEntity> result = testEntityService.findAll();

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("Test Name", result.get(0).getName());
        verify(mockRepository, times(1)).findAll();
    }

    @Test
    void testFindByIdSuccess() {
        when(mockRepository.findById(1L)).thenReturn(Optional.of(mockEntity));

        Optional<TestEntity> result = testEntityService.findById(1L);

        assertTrue(result.isPresent());
        assertEquals("Test Name", result.get().getName());
        verify(mockRepository, times(1)).findById(1L);
    }

    @Test
    void testFindByIdNotFound() {
        when(mockRepository.findById(1L)).thenReturn(Optional.empty());

        Optional<TestEntity> result = testEntityService.findById(1L);

        assertFalse(result.isPresent());
        verify(mockRepository, times(1)).findById(1L);
    }

    @Test
    void testSaveEntitySuccess() {
        when(mockRepository.save(mockEntity)).thenReturn(mockEntity);

        TestEntity result = testEntityService.save(mockEntity);

        assertNotNull(result);
        assertEquals("Test Name", result.getName());
        verify(mockRepository, times(1)).save(mockEntity);
    }

    @Test
    void testUpdateEntitySuccess() {
        TestEntity updatedEntity = new TestEntity(null, "Updated Name");

        when(mockRepository.findById(1L)).thenReturn(Optional.of(mockEntity));
        when(mockRepository.save(mockEntity)).thenReturn(mockEntity);

        TestEntity result = testEntityService.update(1L, updatedEntity);

        assertNotNull(result);
        assertEquals("Updated Name", result.getName());
        verify(mockRepository, times(1)).findById(1L);
        verify(mockRepository, times(1)).save(mockEntity);
    }

    @Test
    void testUpdateEntityNotFound() {
        when(mockRepository.findById(1L)).thenReturn(Optional.empty());

        TestEntity updatedEntity = new TestEntity(null, "Updated Name");

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                testEntityService.update(1L, updatedEntity));

        assertEquals("Entity not found", exception.getMessage());
    }

    @Test
    void testDeleteByIdSuccess() {
        doNothing().when(mockRepository).deleteById(1L);

        testEntityService.deleteById(1L);

        verify(mockRepository, times(1)).deleteById(1L);
    }
}

// Concrete implementation of BaseService for testing
class TestEntityService extends BaseService<TestEntity, Long> {
    public TestEntityService(JpaRepository<TestEntity, Long> repository) {
        super(repository);
    }

    @Override
    protected void updateHook(TestEntity existingEntity, TestEntity updatedEntity) {
        existingEntity.setName(updatedEntity.getName());
    }
}

// TestEntity class for testing
class TestEntity {
    private Long id;
    private String name;

    public TestEntity(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
