package com.example.backend.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.backend.model.Document;
import com.example.backend.model.FileEntity;
import com.example.backend.model.FileType;
import com.example.backend.model.Project;
import com.example.backend.repository.DocumentRepo;
import com.example.backend.repository.FileRepo;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.multipart.MultipartFile;

class DocumentServiceTest {

    @Mock
    private DocumentRepo documentRepository;

    @Mock
    private ProjectService projectService;

    @Mock
    private FileRepo fileRepository;

    @InjectMocks
    private DocumentService documentService;

    private Document mockDocument;
    private Project mockProject;
    private FileEntity mockFileEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        // Initialize mock data
        mockProject = new Project();
        mockProject.setId(1L);

        mockDocument = new Document();
        mockDocument.setId(1L);
        mockDocument.setName("Test Document");
        mockDocument.setProject(mockProject);
        mockDocument.setVersion(1);

        mockFileEntity = new FileEntity();
        mockFileEntity.setName("test-file.txt");
    }

    @Test
    void testCreateDocumentSuccess() {
        when(projectService.findById(1L)).thenReturn(Optional.of(mockProject));
        when(documentRepository.findByProjectIdAndName(1L, "Test Document")).thenReturn(Optional.empty());
        when(documentRepository.save(mockDocument)).thenReturn(mockDocument);

        Document result = documentService.createDocument(1L, mockDocument);

        assertNotNull(result);
        assertEquals(mockProject, result.getProject());
        assertEquals(1, result.getVersion());
        verify(documentRepository, times(1)).save(mockDocument);
    }

    @Test
    void testCreateDocumentProjectNotFound() {
        when(projectService.findById(1L)).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                documentService.createDocument(1L, mockDocument));

        assertEquals("Project not found", exception.getMessage());
        verify(documentRepository, never()).save(any(Document.class));
    }

    @Test
    void testUpdateDocumentSuccess() {
        Document updatedDocument = new Document();
        updatedDocument.setName("Updated Document");
        updatedDocument.setType("text/plain");
        updatedDocument.setContent("Updated Content");

        when(documentRepository.findById(1L)).thenReturn(Optional.of(mockDocument));
        when(documentRepository.save(mockDocument)).thenReturn(mockDocument);

        Document result = documentService.updateDocument(1L, 1L, updatedDocument);

        assertNotNull(result);
        assertEquals("Updated Document", mockDocument.getName());
        assertEquals("text/plain", mockDocument.getType());
        assertEquals("Updated Content", mockDocument.getContent());
        assertEquals(1, mockDocument.getVersion());
    }

    @Test
    void testUpdateDocumentNotFound() {
        when(documentRepository.findById(1L)).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                documentService.updateDocument(1L, 1L, mockDocument));

        assertEquals("Project not found", exception.getMessage());
    }

    @Test
    void testUploadDocumentSuccess() throws IOException {
        MultipartFile mockFile = mock(MultipartFile.class);
        when(mockFile.getOriginalFilename()).thenReturn("test-file.txt");
        when(mockFile.getContentType()).thenReturn("text/plain");
        when(mockFile.getBytes()).thenReturn(new byte[10]);
        when(mockFile.getSize()).thenReturn(10L);

        when(projectService.findById(1L)).thenReturn(Optional.of(mockProject));
        when(fileRepository.save(any(FileEntity.class))).thenReturn(mockFileEntity);
        when(documentRepository.findByProjectIdAndName(1L, "test-file.txt")).thenReturn(Optional.empty());
        when(documentRepository.save(any(Document.class))).thenReturn(mockDocument);

        Document result = documentService.uploadDocument(1L, mockFile, FileType.DOCUMENT);

        assertNotNull(result);
        assertEquals(mockProject, result.getProject());
    }

    @Test
    void testUploadDocumentProjectNotFound() {
        MultipartFile mockFile = mock(MultipartFile.class);

        when(projectService.findById(1L)).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                documentService.uploadDocument(1L, mockFile, FileType.DOCUMENT));

        assertEquals("Project not found", exception.getMessage());
    }

    @Test
    void testDeleteDocumentSuccess() {
        when(documentRepository.existsById(1L)).thenReturn(true);
        doNothing().when(documentRepository).deleteById(1L);

        documentService.deleteById(1L);

        verify(documentRepository, times(1)).deleteById(1L);
    }

    @Test
    void testDeleteDocumentNotFound() {
        when(documentRepository.existsById(1L)).thenReturn(false);

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                documentService.deleteById(1L));

        assertEquals("Document not found", exception.getMessage());
    }

    @Test
    void testFindByProject() {
        when(documentRepository.findByProjectId(1L)).thenReturn(List.of(mockDocument));

        List<Document> result = documentService.findByProject(1L);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("Test Document", result.get(0).getName());
    }

    @Test
    void testArchiveDocumentSuccess() {
        when(documentRepository.findById(1L)).thenReturn(Optional.of(mockDocument));
        when(documentRepository.save(mockDocument)).thenReturn(mockDocument);

        documentService.archiveDocument(1L);

        assertTrue(mockDocument.isArchived());
        verify(documentRepository, times(1)).save(mockDocument);
    }

    @Test
    void testArchiveDocumentNotFound() {
        when(documentRepository.findById(1L)).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                documentService.archiveDocument(1L));

        assertEquals("Document not found", exception.getMessage());
    }
}
