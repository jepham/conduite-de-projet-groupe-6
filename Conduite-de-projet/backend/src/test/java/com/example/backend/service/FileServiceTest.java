package com.example.backend.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.backend.model.FileEntity;
import com.example.backend.model.FileType;
import com.example.backend.model.Issue;
import com.example.backend.model.Project;
import com.example.backend.repository.FileRepo;
import com.example.backend.repository.IssueRepo;
import com.example.backend.repository.ProjectRepo;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.multipart.MultipartFile;

class FileServiceTest {

    @Mock
    private FileRepo fileRepository;

    @Mock
    private ProjectRepo projectRepository;

    @Mock
    private IssueRepo issueRepository;

    @InjectMocks
    private FileService fileService;

    private FileEntity mockFileEntity;
    private Project mockProject;
    private Issue mockIssue;
    private MultipartFile mockMultipartFile;

    @BeforeEach
    void setUp() throws IOException {
        MockitoAnnotations.openMocks(this);

        mockProject = new Project();
        mockProject.setId(1L);

        mockIssue = new Issue();
        mockIssue.setId(1L);

        mockFileEntity = new FileEntity();
        mockFileEntity.setId(1L);
        mockFileEntity.setName("test-file.txt");
        mockFileEntity.setContentType("text/plain");

        mockMultipartFile = mock(MultipartFile.class);
        when(mockMultipartFile.getOriginalFilename()).thenReturn("test-file.txt");
        when(mockMultipartFile.getContentType()).thenReturn("text/plain");
        when(mockMultipartFile.getSize()).thenReturn(100L);
        when(mockMultipartFile.getBytes()).thenReturn("Sample content".getBytes());
    }

    @Test
    void testSaveFileSuccess() throws IOException {
        String createdAt = "2023-01-01T12:00:00.000Z";
        mockFileEntity.setName("custom-name");
        when(projectRepository.findById(1L)).thenReturn(Optional.of(mockProject));
        when(fileRepository.save(any(FileEntity.class))).thenReturn(mockFileEntity);

        FileEntity result = fileService.saveFile(mockMultipartFile, 1L, FileType.DOCUMENT, createdAt, "custom-name");

        assertNotNull(result);
        assertEquals("custom-name", result.getName());
        verify(fileRepository, times(1)).save(any(FileEntity.class));
    }

    @Test
    void testSaveFileProjectNotFound() {
        String createdAt = "2023-01-01T12:00:00.000Z";
        when(projectRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(IllegalArgumentException.class, () ->
                fileService.saveFile(mockMultipartFile, 1L, FileType.DOCUMENT, createdAt, "custom-name")
        );
    }

    @Test
    void testAssociateFileWithIssueSuccess() {
        when(fileRepository.findById(1L)).thenReturn(Optional.of(mockFileEntity));
        when(issueRepository.findById(1L)).thenReturn(Optional.of(mockIssue));
        when(issueRepository.save(mockIssue)).thenReturn(mockIssue);

        FileEntity result = fileService.associateFileWithIssue(1L, 1L);

        assertNotNull(result);
        verify(issueRepository, times(1)).save(mockIssue);
    }

    @Test
    void testAssociateFileWithIssueFileNotFound() {
        when(fileRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(RuntimeException.class, () -> fileService.associateFileWithIssue(1L, 1L));
    }

    @Test
    void testRemoveFileFromIssueSuccess() {
        when(issueRepository.findById(1L)).thenReturn(Optional.of(mockIssue));

        fileService.removeFileFromIssue(1L, 1L);

        verify(issueRepository, times(1)).save(mockIssue);
    }

    @Test
    void testRemoveFileFromIssueIssueNotFound() {
        when(issueRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(RuntimeException.class, () -> fileService.removeFileFromIssue(1L, 1L));
    }

    @Test
    void testGetIssuesByFileSuccess() {
        when(fileRepository.findById(1L)).thenReturn(Optional.of(mockFileEntity));
        when(issueRepository.findByFiles(mockFileEntity)).thenReturn(List.of(mockIssue));

        List<Issue> result = fileService.getIssuesByFile(1L);

        assertNotNull(result);
        assertEquals(1, result.size());
        verify(issueRepository, times(1)).findByFiles(mockFileEntity);
    }

    @Test
    void testGetIssuesByFileNotFound() {
        when(fileRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(RuntimeException.class, () -> fileService.getIssuesByFile(1L));
    }

    @Test
    void testUpdateFileMetadataSuccess() {
        FileEntity updatedFile = new FileEntity();
        updatedFile.setName("updated-name");
        updatedFile.setType(FileType.ARCHIVE);

        when(fileRepository.findById(1L)).thenReturn(Optional.of(mockFileEntity));
        when(fileRepository.save(mockFileEntity)).thenReturn(mockFileEntity);

        FileEntity result = fileService.updateFileMetadata(1L, updatedFile);

        assertNotNull(result);
        assertEquals("updated-name", mockFileEntity.getName());
        assertEquals(FileType.ARCHIVE, mockFileEntity.getType());
    }

    @Test
    void testUpdateFileMetadataNotFound() {
        FileEntity updatedFile = new FileEntity();
        updatedFile.setName("updated-name");

        when(fileRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(RuntimeException.class, () -> fileService.updateFileMetadata(1L, updatedFile));
    }

    @Test
    void testDeleteFile() {
        doNothing().when(fileRepository).deleteById(1L);

        fileService.deleteFile(1L);

        verify(fileRepository, times(1)).deleteById(1L);
    }
}
