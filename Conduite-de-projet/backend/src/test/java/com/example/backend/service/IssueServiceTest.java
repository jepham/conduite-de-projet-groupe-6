package com.example.backend.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.backend.model.Issue;
import com.example.backend.model.Project;
import com.example.backend.repository.IssueRepo;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class IssueServiceTest {

    @Mock
    private IssueRepo issueRepo;

    @Mock
    private ProjectService projectService;

    @InjectMocks
    private IssueService issueService;

    private Issue mockIssue;
    private Project mockProject;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        // Initialize mock data
        mockProject = new Project();
        mockProject.setId(1L);

        mockIssue = new Issue();
        mockIssue.setId(1L);
        mockIssue.setTitle("Test Issue");
        mockIssue.setDescription("Issue Description");
        mockIssue.setPriority("High");
        mockIssue.setStatus("OPEN");
        mockIssue.setProject(mockProject);
    }

    @Test
    void testFindByPriority() {
        when(issueRepo.findByPriority("High")).thenReturn(List.of(mockIssue));

        List<Issue> result = issueService.findByPriority("High");

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("High", result.get(0).getPriority());
        verify(issueRepo, times(1)).findByPriority("High");
    }

    @Test
    void testFindOpenIssues() {
        when(issueRepo.findByStatus("OPEN")).thenReturn(List.of(mockIssue));

        List<Issue> result = issueService.findOpenIssues();

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("OPEN", result.get(0).getStatus());
        verify(issueRepo, times(1)).findByStatus("OPEN");
    }

    @Test
    void testGetResolvedIssues() {
        mockIssue.setStatus("RESOLVED");
        when(issueRepo.findByProjectIdAndStatus(1L, "RESOLVED")).thenReturn(List.of(mockIssue));

        List<Issue> result = issueService.getResolvedIssues(1L);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("RESOLVED", result.get(0).getStatus());
        verify(issueRepo, times(1)).findByProjectIdAndStatus(1L, "RESOLVED");
    }

    @Test
    void testGetUnresolvedIssues() {
        when(issueRepo.findByProjectIdAndStatus(1L, "OPEN")).thenReturn(List.of(mockIssue));

        List<Issue> result = issueService.getUnresolvedIssues(1L);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("OPEN", result.get(0).getStatus());
        verify(issueRepo, times(1)).findByProjectIdAndStatus(1L, "OPEN");
    }

    @Test
    void testGetAllIssues() {
        when(issueRepo.findByProjectId(1L)).thenReturn(List.of(mockIssue));

        List<Issue> result = issueService.getAllIssues(1L);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("Test Issue", result.get(0).getTitle());
        verify(issueRepo, times(1)).findByProjectId(1L);
    }

    @Test
    void testCreateIssueSuccess() {
        when(projectService.findById(1L)).thenReturn(Optional.of(mockProject));
        when(issueRepo.save(any(Issue.class))).thenReturn(mockIssue);

        Issue result = issueService.createIssue(1L, mockIssue);

        assertNotNull(result);
        assertEquals("Test Issue", result.getTitle());
        assertEquals(mockProject, result.getProject());
        verify(issueRepo, times(1)).save(any(Issue.class));
    }

    @Test
    void testCreateIssueProjectNotFound() {
        when(projectService.findById(1L)).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                issueService.createIssue(1L, mockIssue));

        assertEquals("Project not found", exception.getMessage());
        verify(issueRepo, never()).save(any(Issue.class));
    }

    @Test
    void testUpdateIssueSuccess() {
        Issue updatedIssue = new Issue();
        updatedIssue.setTitle("Updated Title");
        updatedIssue.setDescription("Updated Description");
        updatedIssue.setPriority("Medium");
        updatedIssue.setStatus("RESOLVED");

        when(issueRepo.findById(1L)).thenReturn(Optional.of(mockIssue));
        when(issueRepo.save(mockIssue)).thenReturn(mockIssue);

        Issue result = issueService.update(1L, updatedIssue);

        assertNotNull(result);
        assertEquals("Updated Title", mockIssue.getTitle());
        assertEquals("Updated Description", mockIssue.getDescription());
        assertEquals("Medium", mockIssue.getPriority());
        assertEquals("RESOLVED", mockIssue.getStatus());
    }

    @Test
    void testUpdateIssueNotFound() {
        when(issueRepo.findById(1L)).thenReturn(Optional.empty());

        Issue updatedIssue = new Issue();
        updatedIssue.setTitle("Updated Title");

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                issueService.update(1L, updatedIssue));

        assertEquals("Project not found", exception.getMessage());
    }

    @Test
    void testDeleteIssueSuccess() {
        when(issueRepo.existsById(1L)).thenReturn(true);
        doNothing().when(issueRepo).deleteById(1L);

        issueService.deleteById(1L);

        verify(issueRepo, times(1)).deleteById(1L);
    }

    @Test
    void testDeleteIssueNotFound() {
        when(issueRepo.existsById(1L)).thenReturn(false);

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                issueService.deleteById(1L));

        assertEquals("Issue not found", exception.getMessage());
    }
}
