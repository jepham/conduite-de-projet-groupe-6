package com.example.backend.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.backend.model.Project;
import com.example.backend.model.ProjectStatus;
import com.example.backend.model.User;
import com.example.backend.repository.ProjectRepo;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class ProjectServiceTest {

    @Mock
    private ProjectRepo projectRepo;

    @InjectMocks
    private ProjectService projectService;

    private Project mockProject;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        // Initialize mock data
        mockProject = new Project();
        mockProject.setId(1L);
        mockProject.setName("Test Project");
        mockProject.setDescription("Test Description");
        mockProject.setStatus(ProjectStatus.IN_PROGRESS);
    }

    @Test
    void testCreateProject() {
        User user = new User();
        user.setId(1L);
        user.setUsername("user");
        when(projectRepo.save(mockProject)).thenReturn(mockProject);

        Project result = projectService.createProject(user, mockProject);

        assertNotNull(result);
        assertEquals("Test Project", result.getName());
        verify(projectRepo, times(1)).save(mockProject);
    }

    @Test
    void testFindByStatus() {
        when(projectRepo.findByStatus(ProjectStatus.IN_PROGRESS)).thenReturn(List.of(mockProject));

        List<Project> result = projectService.findByStatus(ProjectStatus.IN_PROGRESS);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(ProjectStatus.IN_PROGRESS, result.get(0).getStatus());
        verify(projectRepo, times(1)).findByStatus(ProjectStatus.IN_PROGRESS);
    }

    @Test
    void testArchiveProjectSuccess() {
        when(projectRepo.findById(1L)).thenReturn(Optional.of(mockProject));
        when(projectRepo.save(mockProject)).thenReturn(mockProject);

        projectService.archiveProject(1L);

        assertEquals(ProjectStatus.COMPLETED, mockProject.getStatus());
        verify(projectRepo, times(1)).save(mockProject);
    }

    @Test
    void testArchiveProjectNotFound() {
        when(projectRepo.findById(1L)).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                projectService.archiveProject(1L));

        assertEquals("Project not found", exception.getMessage());
        verify(projectRepo, never()).save(any(Project.class));
    }

    @Test
    void testUpdateProjectSuccess() {
        Project updatedProject = new Project();
        updatedProject.setName("Updated Project");
        updatedProject.setDescription("Updated Description");
        updatedProject.setStatus(ProjectStatus.COMPLETED);

        when(projectRepo.findById(1L)).thenReturn(Optional.of(mockProject));
        when(projectRepo.save(mockProject)).thenReturn(mockProject);

        Project result = projectService.updateProject(1L, updatedProject);

        assertNotNull(result);
        assertEquals("Updated Project", mockProject.getName());
        assertEquals("Updated Description", mockProject.getDescription());
        assertEquals(ProjectStatus.COMPLETED, mockProject.getStatus());
    }

    @Test
    void testUpdateProjectNotFound() {
        Project updatedProject = new Project();
        updatedProject.setName("Updated Project");

        when(projectRepo.findById(1L)).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                projectService.updateProject(1L, updatedProject));

        assertEquals("Project not found", exception.getMessage());
    }

    @Test
    void testGetAllProjects() {
        when(projectRepo.findAll()).thenReturn(List.of(mockProject));

        List<Project> result = projectService.getAllProjects();

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("Test Project", result.get(0).getName());
    }

    @Test
    void testFindProjectSuccess() {
        when(projectRepo.getById(1L)).thenReturn(mockProject);

        Project result = projectService.findProject(1L);

        assertNotNull(result);
        assertEquals("Test Project", result.getName());
    }

    @Test
    void testDeleteProjectSuccess() {
        doNothing().when(projectRepo).deleteById(1L);

        projectService.deleteProject(1L);

        verify(projectRepo, times(1)).deleteById(1L);
    }

    @Test
    void testPreDeleteHookProjectNotFound() {
        when(projectRepo.findById(1L)).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                projectService.preDeleteHook(1L));

        assertEquals("Project not found", exception.getMessage());
    }
}
