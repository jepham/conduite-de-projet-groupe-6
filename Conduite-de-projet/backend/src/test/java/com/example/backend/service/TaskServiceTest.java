package com.example.backend.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.backend.model.Project;
import com.example.backend.model.Task;
import com.example.backend.model.TaskDTO;
import com.example.backend.model.TaskStatus;
import com.example.backend.model.User;
import com.example.backend.repository.ProjectRepo;
import com.example.backend.repository.TaskRepo;
import com.example.backend.repository.UserRepo;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class TaskServiceTest {

    @Mock
    private TaskRepo taskRepo;

    @Mock
    private UserRepo userRepo;

    @Mock
    private ProjectRepo projectRepo;

    @InjectMocks
    private TaskService taskService;

    private Project mockProject;
    private User mockUser;
    private Task mockTask;
    private TaskDTO mockTaskDTO;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        // Initialize mock data
        mockProject = new Project();
        mockProject.setId(1L);

        mockUser = new User();
        mockUser.setId(1L);

        mockTask = new Task();
        mockTask.setId(1L);
        mockTask.setTitle("Test Task");
        mockTask.setProject(mockProject);

        mockTaskDTO = new TaskDTO();
        mockTaskDTO.setTitle("New Task");
        mockTaskDTO.setUserId(1L);
    }

    @Test
    void testCreateTaskSuccess() {
        when(projectRepo.findById(1L)).thenReturn(Optional.of(mockProject));
        when(userRepo.findById(1L)).thenReturn(Optional.of(mockUser));
        when(taskRepo.save(any(Task.class))).thenReturn(mockTask);

        Task result = taskService.createTask(1L, mockTaskDTO);

        assertNotNull(result);
        assertEquals("Test Task", result.getTitle());
        assertEquals(mockProject, result.getProject());
        verify(taskRepo, times(1)).save(any(Task.class));
    }

    @Test
    void testCreateTaskProjectNotFound() {
        when(projectRepo.findById(1L)).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                taskService.createTask(1L, mockTaskDTO));

        assertEquals("Project not found", exception.getMessage());
        verify(taskRepo, never()).save(any(Task.class));
    }

    @Test
    void testCreateTaskUserNotFound() {
        when(projectRepo.findById(1L)).thenReturn(Optional.of(mockProject));
        when(userRepo.findById(1L)).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                taskService.createTask(1L, mockTaskDTO));

        assertEquals("User not found", exception.getMessage());
    }

    @Test
    void testUpdateTaskSuccess() {
        TaskDTO updatedTaskDTO = new TaskDTO();
        updatedTaskDTO.setTitle("Updated Task");
        updatedTaskDTO.setDescription("Updated Description");
        updatedTaskDTO.setStatus(TaskStatus.IN_PROGRESS);
        updatedTaskDTO.setUserId(1L);
        updatedTaskDTO.setCompleted(true);

        when(taskRepo.findById(1L)).thenReturn(Optional.of(mockTask));
        when(userRepo.findById(1L)).thenReturn(Optional.of(mockUser));
        when(taskRepo.save(mockTask)).thenReturn(mockTask);

        Task result = taskService.update(1L, updatedTaskDTO);

        assertNotNull(result);
        assertEquals("Updated Task", mockTask.getTitle());
        assertEquals("Updated Description", mockTask.getDescription());
        assertEquals(TaskStatus.COMPLETED, mockTask.getStatus());
        assertEquals(mockUser, mockTask.getUserDeveloping());
        assertTrue(mockTask.isCompleted());
    }

    @Test
    void testUpdateTaskNotFound() {
        when(taskRepo.findById(1L)).thenReturn(Optional.empty());

        TaskDTO updatedTaskDTO = new TaskDTO();
        updatedTaskDTO.setTitle("Updated Task");

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                taskService.update(1L, updatedTaskDTO));

        assertEquals("Task not found", exception.getMessage());
    }

    @Test
    void testUpdateTaskUserNotFound() {
        TaskDTO updatedTaskDTO = new TaskDTO();
        updatedTaskDTO.setUserId(2L);

        when(taskRepo.findById(1L)).thenReturn(Optional.of(mockTask));
        when(userRepo.findById(2L)).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                taskService.update(1L, updatedTaskDTO));

        assertEquals("User not found", exception.getMessage());
    }

    @Test
    void testDeleteTaskSuccess() {
        when(taskRepo.existsById(1L)).thenReturn(true);
        doNothing().when(taskRepo).deleteById(1L);

        taskService.deleteById(1L);

        verify(taskRepo, times(1)).deleteById(1L);
    }

    @Test
    void testDeleteTaskNotFound() {
        when(taskRepo.existsById(1L)).thenReturn(false);

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                taskService.deleteById(1L));

        assertEquals("Task not found", exception.getMessage());
    }

    @Test
    void testGetCompletedTasks() {
        when(taskRepo.findByProjectIdAndStatus(1L, TaskStatus.COMPLETED)).thenReturn(List.of(mockTask));

        List<Task> result = taskService.getCompletedTasks(1L);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("Test Task", result.get(0).getTitle());
    }

    @Test
    void testGetPendingTasks() {
        when(taskRepo.findByProjectIdAndStatus(1L, TaskStatus.PENDING)).thenReturn(List.of(mockTask));

        List<Task> result = taskService.getPendingTasks(1L);

        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    void testGetInProgressTasks() {
        when(taskRepo.findByProjectIdAndStatus(1L, TaskStatus.IN_PROGRESS)).thenReturn(List.of(mockTask));

        List<Task> result = taskService.getInProgressTasks(1L);

        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    void testGetAllTasks() {
        when(taskRepo.findByProjectId(1L)).thenReturn(List.of(mockTask));

        List<Task> result = taskService.getAllTasks(1L);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("Test Task", result.get(0).getTitle());
    }
}
