package com.example.backend.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.backend.model.Project;
import com.example.backend.repository.TestRepo;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class TestServiceTest {

    @Mock
    private TestRepo testRepo;

    @Mock
    private ProjectService projectService;

    @InjectMocks
    private TestService testService;

    private Project mockProject;
    private com.example.backend.model.Test mockTest;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        // Initialize mock data
        mockProject = new Project();
        mockProject.setId(1L);

        mockTest = new com.example.backend.model.Test();
        mockTest.setId(1L);
        mockTest.setProject(mockProject);
    }

    @Test
    void testCreateTestSuccess() {
        when(projectService.findById(1L)).thenReturn(Optional.of(mockProject));
        when(testRepo.save(mockTest)).thenReturn(mockTest);

        com.example.backend.model.Test result = testService.createTest(1L, mockTest);

        assertNotNull(result);
        assertEquals(mockProject, result.getProject());
        verify(testRepo, times(1)).save(mockTest);
    }

    @Test
    void testCreateTestProjectNotFound() {
        when(projectService.findById(1L)).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () -> testService.createTest(1L, mockTest));
        assertEquals("Project not found", exception.getMessage());
    }

    @Test
    void testDeleteByIdSuccess() {
        when(testRepo.existsById(1L)).thenReturn(true);
        doNothing().when(testRepo).deleteById(1L);

        testService.deleteById(1L);

        verify(testRepo, times(1)).deleteById(1L);
    }

    @Test
    void testDeleteByIdNotFound() {
        when(testRepo.existsById(1L)).thenReturn(false);

        RuntimeException exception = assertThrows(RuntimeException.class, () -> testService.deleteById(1L));
        assertEquals("Test not found", exception.getMessage());
    }

    @Test
    void testGetAllTests() {
        when(testRepo.findByProjectId(1L)).thenReturn(List.of(mockTest));

        List<com.example.backend.model.Test> result = testService.getAllTests(1L);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(mockTest, result.get(0));
        verify(testRepo, times(1)).findByProjectId(1L);
    }
}
