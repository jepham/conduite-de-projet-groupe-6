package com.example.backend.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.backend.model.User;
import com.example.backend.repository.UserRepo;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class UserServiceTest {

    @Mock
    private UserRepo userRepo;

    @InjectMocks
    private UserService userService;

    private User mockUser;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        // Initialize mock data
        mockUser = new User();
        mockUser.setId(1L);
        mockUser.setUsername("testUser");
        mockUser.setEmail("test@example.com");
    }

    @Test
    void testGetUserByUsernameSuccess() {
        when(userRepo.findByUsername("testUser")).thenReturn(Optional.of(mockUser));

        User result = userService.getUserByUsername("testUser");

        assertNotNull(result);
        assertEquals("testUser", result.getUsername());
        verify(userRepo, times(1)).findByUsername("testUser");
    }

    @Test
    void testGetUserByUsernameNotFound() {
        when(userRepo.findByUsername("nonExistentUser")).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                userService.getUserByUsername("nonExistentUser"));

        assertEquals("User not found with username: nonExistentUser", exception.getMessage());
        verify(userRepo, times(1)).findByUsername("nonExistentUser");
    }

    @Test
    void testGetUserByIdSuccess() {
        when(userRepo.findById(1L)).thenReturn(Optional.of(mockUser));

        User result = userService.getUserById(1L);

        assertNotNull(result);
        assertEquals(1L, result.getId());
        verify(userRepo, times(1)).findById(1L);
    }

    @Test
    void testGetUserByIdNotFound() {
        when(userRepo.findById(1L)).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                userService.getUserById(1L));

        assertEquals("User not found with ID: 1", exception.getMessage());
        verify(userRepo, times(1)).findById(1L);
    }

    @Test
    void testGetUserByEmailSuccess() {
        when(userRepo.findByEmail("test@example.com")).thenReturn(Optional.of(mockUser));

        User result = userService.getUserByEmail("test@example.com");

        assertNotNull(result);
        assertEquals("test@example.com", result.getEmail());
        verify(userRepo, times(1)).findByEmail("test@example.com");
    }

    @Test
    void testGetUserByEmailNotFound() {
        when(userRepo.findByEmail("nonExistent@example.com")).thenReturn(Optional.empty());

        RuntimeException exception = assertThrows(RuntimeException.class, () ->
                userService.getUserByEmail("nonExistent@example.com"));

        assertEquals("User not found with email: nonExistent@example.com", exception.getMessage());
        verify(userRepo, times(1)).findByEmail("nonExistent@example.com");
    }

    @Test
    void testGetAllUsers() {
        when(userRepo.findAll()).thenReturn(List.of(mockUser));

        List<User> result = userService.getAllUsers();

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("testUser", result.get(0).getUsername());
        verify(userRepo, times(1)).findAll();
    }
}
