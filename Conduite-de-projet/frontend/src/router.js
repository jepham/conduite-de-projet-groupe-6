import { createRouter, createWebHistory } from 'vue-router';
import Authentification from './components/Authentification.vue';
import Dashboard from './components/Dashboard.vue';
import Issues from './components/features/Issues.vue';
import Tasks from './components/features/Tasks.vue';
import Releases from './components/features/Releases.vue';
import Tests from './components/features/Tests.vue';
import Documentation from './components/features/Documentation.vue';
import Team from './components/Team.vue';
import Settings from './components/Settings.vue';
import Projects from './components/Projects.vue';

const routes = [
  { path: '/', component: Authentification },
  { path: '/projects', component: Projects, meta: { requiresAuth: true } },
  { path: '/projects/:id/dashboard', component: Dashboard, meta: { requiresAuth: true } },
  { path: '/projects/:id/issues', component: Issues, meta: { requiresAuth: true } },
  { path: '/projects/:id/tasks', component: Tasks, meta: { requiresAuth: true } },
  { path: '/projects/:id/releases', component: Releases, meta: { requiresAuth: true } },
  { path: '/projects/:id/tests', component: Tests, meta: { requiresAuth: true } },
  { path: '/projects/:id/documents', component: Documentation, meta: { requiresAuth: true } },
  { path: '/team', component: Team, meta: { requiresAuth: true } },
  { path: '/settings', component: Settings, meta: { requiresAuth: true } },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  const token = localStorage.getItem('authToken'); 

  if (to.meta.requiresAuth && !token) {
    // Redirect to login if not authenticated
    next('/');
  } else {
    next(); // Allow navigation
  }
});

export default router;
