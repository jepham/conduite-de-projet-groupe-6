import axios from 'axios';

const API_URL = 'http://localhost:8081';

const apiClient = axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
    'withCredentials': true,
  },
});

const authClient = axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
    'withCredentials': true,
  },
});

export const projectService = {
  createProject: async (username,project) => {
    console.log('Service sending project:', project);
    try {
      const response = await apiClient.post(`/projects/${username}/project`, project);
      return response;
    } catch (error) {
      console.error('Service create error:', error);
      throw error;
    }
  },
  getProjectsByID: async (username) => {
    try {
      const response = await apiClient.get(`/projects/${username}/project`);
      return response;
    } catch (error) {
      console.error('Service create error:', error);
      throw error;
    }
  },
  deleteProject: async (id) => {
    try {
      const response = await apiClient.delete(`/projects/${id}`);
      return response;
    } catch (error) {
      console.error('Service delete error:', error);
      throw error;
    }
  },
  getAllProjects: async () => {
    try {
      const response = await apiClient.get('/projects');
      return response;
    } catch (error) {
      console.error('Service get all error:', error);
      throw error;
    }
  },
getUserID: async (user) => {
  try {
    const response = await apiClient.get(`/api/users/`,user);
    return response.data;
  } catch (error) {
    console.error('Error assigning user to project:', error);
    throw error;
  }
},
  archiveProject: async (id) => {
    try {
      const response = await apiClient.post(`/projects/${id}/archive`);
      return response;
    } catch (error) {
      console.error('Service archive error:', error);
      throw error;
    }
  },
  getProjectsByStatus: async (status) => {
    try {
      const response = await apiClient.get(`/projects/status/${status}`);
      return response;
    } catch (error) {
      console.error('Service get projects by status error:', error);
      throw error;
    }
  },
  updateProject: async (id, updatedData) => {
    console.log('Service updating project:', id, updatedData);
    try {
      const response = await apiClient.put(`/projects/${id}`, updatedData);
      return response;
    } catch (error) {
      console.error('Service update error:', error);
      throw error;
    }
  }
};

export const githubAuthService = {
  handleCallback: async (code) => {
    try {
      const response = await authClient.get(`/api/auth/github/callback?code=${code}`);
      return response.data;
    } catch (error) {
      console.error('GitHub callback error:', error);
      throw error;
    }
  },
  getUserInfo: async (accessToken) => {
    try {
      const response = await authClient.get('/api/auth/github/user', {
        headers: {
          'Authorization': `Bearer ${accessToken}`
        }
      });
      return response.data;
    } catch (error) {
      console.error('Get user info error:', error);
      throw error;
    }
  }
};

export const taskService = {
  getAllTasks(projectId) {
    return apiClient.get(`/projects/${projectId}/tasks`);
  },
  createTask(projectId, taskData) {
    return apiClient.post(`/projects/${projectId}/tasks`, taskData);
  },
  updateTask(projectId, id, task) {
    return apiClient.put(`/projects/${projectId}/tasks/${id}`, task);
  },
  deleteTask(projectId, id) {
    return apiClient.delete(`/projects/${projectId}/tasks/${id}`);
  },
  getCompletedTasks(projectId) {
    return apiClient.get(`/projects/${projectId}/tasks/completed`);
  },
  getPendingTasks(projectId) {
    return apiClient.get(`/projects/${projectId}/tasks/pending`);
  },
  getInProgressTasks(projectId) {
    return apiClient.get(`/projects/${projectId}/tasks/progress`);
  },
  // Nouvelle fonction pour récupérer tous les utilisateurs
  getUsers: async () => {
    try {
      const response = await apiClient.get('/users/all');
      return response.data;
    } catch (error) {
      console.error('Error fetching users:', error);
      throw error;
    }
  }

};
export const issueService = {
  getIssuesByProject(projectId) {
    return apiClient.get(`/projects/${projectId}/issues`);
  },

  getAllIssues(projectId) {
    return apiClient.get(`/projects/${projectId}/issues`);
  },
  getIssuesByPriority(projectId, priority) {
    return apiClient.get(`/projects/${projectId}/issues/priority/${priority}`);
  },

  getOpenIssues(projectId) {
    return apiClient.get(`/projects/${projectId}/issues/open`);
  },

  createIssue(projectId, issue) {
    return apiClient.post(`/projects/${projectId}/issues`, issue);
  },

  updateIssue(projectId, issueId, issue) {
    return apiClient.put(`/projects/${projectId}/issues/${issueId}`, issue);
  },

  deleteIssue(projectId, issueId) {
    return apiClient.delete(`/projects/${projectId}/issues/${issueId}`);
  }
};


export const documentService = {
  getDocumentsByProject(projectId) {
    return apiClient.get(`/projects/${projectId}/documents`, projectId);
  },

  createDocument(projectId, document) {
    return apiClient.post(`/projects/${projectId}/documents`, document);
  },
  updateDocument(projectId, documentId, document) {
    return apiClient.put(`/projects/${projectId}/documents/${documentId}`, document);
  },
  archiveDocument(projectId, id) {
    return apiClient.post(`/projects/${projectId}/documents/archive/${id}`, projectId, id);
  },

  unarchiveDocument(projectId, id) {
    return apiClient.post(`/projects/${projectId}/documents/unarchive/${id}`, projectId, id);
  },
  deleteDocument(projectId, id) {
    return apiClient.delete(`/projects/${projectId}/documents/${id}`, id);
  }
};



export const fileService = {
  uploadFile(projectId, type, file, createdAt, contentName) {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('name', contentName);
    formData.append('createdAt', createdAt);
    
    return apiClient.post(`/api/${projectId}/files/${type}`, formData, {
      headers: { 'Content-Type': 'multipart/form-data' },
    }).then(response => {
      return { response, contentName };
    });
  },

  getFilesByType(projectId, type) {
    return apiClient.get(`/api/projects/${projectId}/files/${type}`);
  },

  getFile(projectId, type, fileId) {
    return apiClient.get(`/api/projects/${projectId}/files/${type}/${fileId}`, {
      responseType: 'blob',
    });
  },
  getIssuesByFile(fileId) {
    return apiClient.get(`/api/files/${fileId}/issues`);
  },
  deleteFile(projectId, type, fileId) {
    return apiClient.delete(`/api/projects/${projectId}/files/${type}/${fileId}`);
  },

  associateFileWithIssue(issueId, fileId) {
    return apiClient.post(`/api/issues/${issueId}/files/${fileId}`);
  },

  removeFileFromIssue(issueId, fileId) {
    return apiClient.delete(`/api/issues/${issueId}/files/${fileId}`);
  },

  getFilesByIssue(issueId) {
    return apiClient.get(`/api/issues/${issueId}/files`);
  },

  updateFileMetadata(fileId, updatedFile) {
    return apiClient.put(`/api/files/${fileId}`, updatedFile);
  },
};



export const testService = {
  createTest(projectId, testData, repoUrl, results) {
    return apiClient.post(`/api/projects/${projectId}/tests`, testData, repoUrl, results);
  },

  getTestById(projectId, testId) {
    return apiClient.get(`/api/projects/${projectId}/tests/${testId}`);
  },

  deleteTest(projectId, testId) {
    return apiClient.delete(`/api/projects/${projectId}/tests/${testId}`);
  },

  getAllTests(projectId) {
    return apiClient.get(`/api/projects/${projectId}/tests`);
  }

};



// Ajouter le token automatiquement pour chaque requête
//apiClient.interceptors.request.use((config) => {
//  const token = localStorage.getItem('authToken'); // Récupère le token du localStorage
//  if (token) {
//    config.headers.Authorization = `Bearer ${token}`; // Ajoute le token dans l'en-tête Authorization
//  }
//  return config;
//}, (error) => {
//  return Promise.reject(error); // Gère les erreurs de configuration quant elles se produisent
//});

// Intercepter les réponses pour gérer les erreurs 401
//apiClient.interceptors.response.use(
//  (response) => {
//    return response; 
//  },
//  (error) => {
//    if (error.response && error.response.status === 401) {
//      alert('Your session has expired. Please log in again.');
//      console.error('Unauthorized: Token may be invalid or expired');
//      localStorage.removeItem('authToken'); // Supprime le token invalide ou expiré
//      window.location.href = '/'; // Redirige vers la page de login
//    }
//    return Promise.reject(error); // Rejette toutes les autres erreurs
//  }
//);

export const authentificationService = {
  // Récupérer un utilisateur par son nom d'utilisateur
  getUserByUsername: async (username) => {
    try {
      const response = await apiClient.get(`/users/username/${username}`);
      return response.data;
    } catch (error) {
      console.error('Error fetching user:', error);
      throw error;
    }
  },
  login: async (username, password) => {
    try {
      const response = await apiClient.post('/api/auth/login', null, {
        params: { username, password },
      });

      console.log('Response from backend:', response.data);  // Affichage de la réponse du backend

      if (response.data) {
        // Si le login est réussi, sauvegarder les données nécessaires dans le localStorage
        const user = {
          username: response.data.username || 'Guest',
        };

        // Sauvegarder les informations de l'utilisateur et le token dans le localStorage
        localStorage.setItem('user', JSON.stringify(user));
        localStorage.setItem('authToken', response.data.token); // On suppose que 'token' est renvoyé dans la réponse

        return response.data;  // Retourner les données du login
      }

    } catch (error) {
      console.error('Login failed:', error);
      throw error;
    }
  },

  register: async (user) => {
    try {
      const response = await apiClient.post('/api/auth/register', user);
      return response.data;
    } catch (error) {
      console.error('Register failed:', error);
      throw error;
    }
  },
};