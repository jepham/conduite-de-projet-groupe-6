-- Table for Users (unchanged)
CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    username VARCHAR(255) NOT NULL UNIQUE,
    email VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL
);

-- Projects table (slightly modified)
CREATE TABLE projects (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description TEXT,
    status VARCHAR(50),
    created_at TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE
);



-- Table Teams
CREATE TABLE teams (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description TEXT,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

-- Table between Users and Teams
CREATE TABLE user_teams (
    user_id BIGINT NOT NULL,
    team_id BIGINT NOT NULL,
    PRIMARY KEY (user_id, team_id),
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (team_id) REFERENCES teams(id) ON DELETE CASCADE
);

-- Table between Teams and Projects
CREATE TABLE team_projects (
    team_id BIGINT NOT NULL,
    project_id BIGINT NOT NULL,
    PRIMARY KEY (team_id, project_id),
    FOREIGN KEY (team_id) REFERENCES teams(id) ON DELETE CASCADE,
    FOREIGN KEY (project_id) REFERENCES projects(id) ON DELETE CASCADE
);

-- Issues table
CREATE TABLE issues (
    id BIGSERIAL PRIMARY KEY,
    title VARCHAR(255),
    description TEXT,
    priority VARCHAR(50),
    status VARCHAR(50),
    resolved BOOLEAN DEFAULT FALSE,
    created_at TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE,
    project_id BIGINT NOT NULL REFERENCES projects(id) ON DELETE CASCADE
);

-- Tasks table
CREATE TABLE tasks (
    id BIGSERIAL PRIMARY KEY,
    title VARCHAR(255),
    description TEXT,
    completed BOOLEAN DEFAULT FALSE,
    status VARCHAR(50),  -- Changed from enum to VARCHAR
    deadline TIMESTAMP WITH TIME ZONE,
    creation_date TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE,
    user_developing_id BIGINT,
    project_id BIGINT NOT NULL REFERENCES projects(id) ON DELETE CASCADE,
    FOREIGN KEY (user_developing_id) REFERENCES users(id) ON DELETE SET NULL
);

-- Documents table
CREATE TABLE documents (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(255),
    type VARCHAR(50),
    archived BOOLEAN DEFAULT FALSE,
    file_path VARCHAR(255),
    version INTEGER DEFAULT 1,
    content TEXT,
    created_at TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE,
    project_id BIGINT NOT NULL REFERENCES projects(id) ON DELETE CASCADE
);

-- Files table
CREATE TABLE files (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(255),
    content_type VARCHAR(100),
    size BIGINT,
    data BYTEA,
    type VARCHAR(50),  -- Changed from enum to VARCHAR
    created_at TIMESTAMP WITH TIME ZONE,
    project_id BIGINT NOT NULL REFERENCES projects(id) ON DELETE CASCADE
);


-- Tests table
CREATE TABLE tests (
    id BIGSERIAL PRIMARY KEY,
    title VARCHAR(255),
    description TEXT,
    repoUrl TEXT,
    result TEXT,
    status VARCHAR(50),  -- Changed from enum to VARCHAR
    created_at TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE,
    project_id BIGINT NOT NULL REFERENCES projects(id) ON DELETE CASCADE
);
-- New junction table for the many-to-many relationship between Users and Projects
CREATE TABLE project_user (
    project_id BIGINT NOT NULL,
    user_id INT NOT NULL,
    PRIMARY KEY (project_id, user_id),
    FOREIGN KEY (project_id) REFERENCES projects(id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

-- Junction table for Issues and Files
CREATE TABLE issue_files (
    issue_id BIGINT NOT NULL,
    file_id BIGINT NOT NULL,
    PRIMARY KEY (issue_id, file_id),
    FOREIGN KEY (issue_id) REFERENCES issues(id) ON DELETE CASCADE,
    FOREIGN KEY (file_id) REFERENCES files(id) ON DELETE CASCADE
);

-- Junction table for the many-to-many relationship between Tasks and Users
CREATE TABLE task_user (
    task_id BIGINT NOT NULL,
    user_id BIGINT ,
    PRIMARY KEY (task_id, user_id),
    FOREIGN KEY (task_id) REFERENCES tasks(id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

CREATE OR REPLACE FUNCTION update_updated_at_column()
RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = CURRENT_TIMESTAMP;
    RETURN NEW;
END;

CREATE TRIGGER set_updated_at_project
BEFORE UPDATE ON Projects
FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

CREATE TRIGGER set_updated_at_team
BEFORE UPDATE ON teams
FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

CREATE TRIGGER set_updated_at_document
BEFORE UPDATE ON Documents
FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

CREATE TRIGGER set_updated_at_issue
BEFORE UPDATE ON Issues
FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

CREATE TRIGGER set_updated_at_task
BEFORE UPDATE ON Tasks
FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();


-- Create indexes for foreign keys and frequently queried fields
CREATE INDEX idx_tasks_project_id ON tasks(project_id);
CREATE INDEX idx_tasks_user_developing_id ON tasks(user_developing_id);
CREATE INDEX idx_issues_project_id ON issues(project_id);
CREATE INDEX idx_documents_project_id ON documents(project_id);
CREATE INDEX idx_files_project_id ON files(project_id);
CREATE INDEX idx_tests_project_id ON tests(project_id);

CREATE INDEX idx_user_teams_user_id ON user_teams(user_id);
CREATE INDEX idx_user_teams_team_id ON user_teams(team_id);
CREATE INDEX idx_team_projects_team_id ON team_projects(team_id);
CREATE INDEX idx_team_projects_project_id ON team_projects(project_id);

-- Create indexes for status fields that are likely to be queried often
CREATE INDEX idx_projects_status ON projects(status);
CREATE INDEX idx_tasks_status ON tasks(status);
CREATE INDEX idx_tests_status ON tests(status);