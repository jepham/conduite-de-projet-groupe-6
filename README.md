# Conduite de projet - Groupe 6


## Run the project

```
frontend :
npm install
npm install lucide-vue-next
npm install vue-router@4
npm install axios
npm run dev

Docker PostgreSQL :
LAUNCH DOCKER : docker-compose up -d
CLOSE DOCKER : docker-compose down
DELETE DOCKER VOLUME : docker volume rm postgres_data
ou docker-compose down -v
ENTER INTO THE DB : docker exec -it my_postgres psql -U postgres -d database
SEE LOGS : docker logs my_postgres


backend : 
(lancer le docker avant les commandes du back : docker-compose up -d)
mvn clean install
mvn spring-boot:run
    or if cremi
mvn spring-boot:run -Dspring-boot.run.arguments=--server.port=8081



(With API Client like Postman) (Just to test, it will later be implemented in the web application)
GET : http://localhost:8080/userstories
POST : http://localhost:8080/userstories with Body like : {
  "description": "New user story",
  "priority": "High"
}

```
